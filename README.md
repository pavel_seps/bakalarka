# README #

Made as a bachelor's thesis at University of South Bohemia.

## Set up server ##
* copy Config/AppConfig.template.config to Config/AppConfig.config
* set up Config/AppConfig.config variables
* download all packages
* run Database migrations and set up database server
* run App

## Set up Front End ##
* copy web/config/dev.env.template.js to web/config/dev.env.js
* copy web/config/prod.env.template.js to web/config/prod.env.js
* set up server url
* run 'cd /web'
* run 'npm install'
* run 'npm install vue'
* run 'npm run dev'
* for build run 'npm run build'

## How to Migrate database ##

#### Enable new migration ####
* PM> Enable-Migrations -ContextTypeName <NEW_MODEL> -Force -MigrationsDirectory <NEW_MODEL_FOLDER>
#### Create migration ####
* PM> Add-Migration <NAME> -ConfigurationTypeName <CONFIG_FILE>
#### Update database ####
* PM> Update-Database -ConfigurationTypeName <CONFIG_FILE>