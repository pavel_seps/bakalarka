﻿using Bakalarka.app;
using Bakalarka.app.Communication;
using Bakalarka.app.MapModels.Questions;
using Bakalarka.app.RealEstateModels;
using Bakalarka.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using Newtonsoft.Json;

namespace TestConsole
{
    /// <summary>
    /// Testing and fast debugging component via console
    /// </summary>
    class Program
    {

        static void Main(string[] args)
        {
            testGetQuestion();

            Console.ReadKey();
        }


        private static void testTransfer()
        {
            Transfer<Tuple<double, double>> t = new Transfer<Tuple<double, double>>();
            t.Message = "Done";
            t.Data = new Tuple<double, double>(48.9883604, 14.4708852);
            Console.WriteLine(t.Stringify());
            Transfer<Tuple<double, double>> r = new Transfer<Tuple<double, double>>();

            r.Parse(t.Stringify());
            Console.WriteLine(r.Message+"\t"+r.Data.ToString());
        }

        private static void testGetLocationByDistance()
        {
            Tuple<double, double> start = new Tuple<double, double>(48.9883604, 14.4708852);
            Tuple<double, double> end = Helpers.GetLocationByDistance(start, 200, 90);
            Console.WriteLine("Distance: " + Math.Round(Helpers.GetDistance(start, end)));
            Console.WriteLine("Start: " + start.ToString());
            Console.WriteLine("End: " + end.ToString());
            Console.WriteLine();
            Console.WriteLine("0: " + Helpers.GetLocationByDistance(start, 200, 0));
            Console.WriteLine("90: " + Helpers.GetLocationByDistance(start, 200, 90));
            Console.WriteLine("180: " + Helpers.GetLocationByDistance(start, 200, 180));
            Console.WriteLine("270: " + Helpers.GetLocationByDistance(start, 200, 270));
        }

        private static void testRateAlgorythm()
        {
            int errors = 0;
            double maxDelimiter = 0;

            for (int realEstateCount = 0; realEstateCount <= 100; realEstateCount++)
            {

                for (int answerCount = 1; answerCount <= 10; answerCount++)
                {

                    int answer = answerCount;
                    double delimiterDouble = (double)realEstateCount / answerCount;
                    int delimiterCeil = (int)Math.Ceiling((double)realEstateCount / answerCount);
                    int delimiterFloor = (int)Math.Floor((double)realEstateCount / answerCount);
                    int delimiterAnswer = 0;
                    delimiterAnswer = delimiterDouble % 1 > 0.7 ? delimiterCeil : delimiterFloor;
                    //max is 10 answers
                    delimiterAnswer = delimiterAnswer == 0 ? 1 : delimiterAnswer;

                    string row = "";
                    //**
                    for (int i = 0; i < realEstateCount; i++)
                    {
                        row += answer + ", ";
                        if (answer > 1 && i % delimiterAnswer == delimiterAnswer - 1)
                        {
                            answer--;
                        }
                    }
                    //**
                    if (answer != 1 && realEstateCount > answerCount)
                    {
                        Console.WriteLine("====== Real Estate: " + realEstateCount + "\t Answers: " + answerCount + "\t Delimiter: " + (double)realEstateCount / answerCount + $" ({delimiterAnswer})");
                        Console.WriteLine(row);
                        Console.WriteLine();
                        Console.WriteLine();
                        if (maxDelimiter < delimiterDouble % 1)
                        {
                            maxDelimiter = delimiterDouble % 1;
                        }
                        errors++;
                    }
                }
            }
            Console.WriteLine("Errors: " + errors);
            Console.WriteLine("Max delimiter: " + maxDelimiter);

        }

        private static void testInterest()
        {
            InterestModel model = new InterestModel();
            RealEstate test = (RealEstate)model.GetInterest(new RealEstate() { Location = new LocationData() { lat = 48.9883604, lng = 14.4708852 } });

            foreach(Interest interest in test.Interests)
            {
                Console.WriteLine($"{interest.Id}\n{interest.Name}\n{interest.Value}\n\n");
            }
        }

        private static void testSave()
        {
            Save save = new Save();
            Console.WriteLine(save.Load("12356"));
        }


        private static async Task testGetQuestionSearch()
        {
            Search search = new Search();
            search.SetLocation(new LocationData() { lat = 48.9883604, lng = 14.4708852, radius = 1000 });

            foreach (QuestionData question in await search.GetQuestions())
            {
                Console.WriteLine(question.Question);

                foreach (var answer in question.Answers)
                {
                    Console.Write(answer.Item2 + ", ");
                }
                Console.WriteLine();
            }

            search.SetAnswers(JsonConvert.DeserializeObject<List<AnswerData>>("[{\"Id\":\"restaurantQuestion\", \"Answer\":1,\"Important\":false},{\"Id\":\"shopQuestion\", \"Answer\":2,\"Important\":false},{\"Id\":\"bakeryQuestion\", \"Answer\":2,\"Important\":false}]"));

            Console.WriteLine("Done");
        }

        private async static void testGetQuestion()
        {
            MapDataModel questions = new MapDataModel();
            LocationData location = new LocationData() { lat = 48.9883604, lng = 14.4708852, radius = 1000 };

            foreach (Question question in await questions.GetQuestions(location))
            {
                Console.WriteLine(question.QuestionText??"Undefind");

                foreach (var answer in question.Answers)
                {
                    Console.Write(answer.Item2 + ", ");
                }
                Console.WriteLine();
            }
        }

        private static void testGetRealestate()
        {
            RealEstateDataModel realEstateDataModel = new RealEstateDataModel();
            LocationData location = new LocationData() { lat = 48.9883604, lng = 14.4708852, radius = 1000 };
             
            foreach (ExternalData realityResult in new List<ExternalData>(realEstateDataModel.GetRealEstates(location)))
            {
                Console.WriteLine(realityResult.Name);
            }
        }

        private static async void testRating()
        {
            RealEstateDataModel realEstateDataModel = new RealEstateDataModel();
            List<Tuple<string, int>> answers = new List<Tuple<string, int>>();
            MapDataModel questions = new MapDataModel();
            LocationData location = new LocationData() { lat = 48.9883604, lng = 14.4708852, radius = 1000 };
            Rating rating = new Rating();
            
            foreach (RealEstate realEstate in rating.RateQuestions(new List<ExternalData>(realEstateDataModel.GetRealEstates(location)), new List<Question>(await questions.GetQuestions(location))))
            {
                Console.WriteLine(realEstate.Name);

                foreach(var locationData in realEstate.PoiData)
                {
                    Console.WriteLine(locationData.Question + "\t" + locationData.Answer);
                }
                Console.WriteLine();
            }
        }

        private static async void testComparation()
        {
            MapDataModel questions = new MapDataModel();
            Rating rating = new Rating();
            RealEstateDataModel realEstateDataModel = new RealEstateDataModel();
            LocationData location = new LocationData() { lat = 48.9883604, lng = 14.4708852, radius = 1000 };
            Comparation compare = new Comparation();

            List<ExternalData> result = new List<ExternalData>();

            result = compare.Compare(rating.RateQuestions(new List<ExternalData>(realEstateDataModel.GetRealEstates(location)), new List<Question>(await questions.GetQuestions(location))), new List<Question>(await questions.GetQuestions(location)), 0);

            foreach (RealEstate realityResult in result)
            {
                Console.WriteLine(realityResult.Name + ": " + realityResult.Percentage + "%");
            }
        }
    }
}
