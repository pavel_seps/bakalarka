﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bakalarka.app;
using Bakalarka.app.Communication;

namespace TestBakalarka
{
    /// <summary>
    /// RealEstateTest
    /// </summary>
    [TestClass]
    public class RealEstateTest
    {
        private RealEstate realEstate;

        public RealEstateTest()
        {
            realEstate = new RealEstate() {
                Id = "test-50",
                Percentage = 66,
                Location = new LocationData() {  lat = 50, lng = 60, radius = 5000},
                Name = "RealEstate",
                PoiData = new List<PoiData>()
            };

            realEstate.PoiData.Add(new PoiData() { Id = "poi1", Question = "question1", Answer = 2 });
            realEstate.PoiData.Add(new PoiData() { Id = "poi2", Question = "question2", Answer = 1 });
            realEstate.PoiData.Add(new PoiData() { Id = "poi3", Question = "question3", Answer = 5 });
        }

        [TestMethod]
        public void TestCreation()
        {
            Assert.AreEqual("test-50", realEstate.Id);
            Assert.AreEqual(66, realEstate.Percentage);

            Assert.AreEqual(50, realEstate.Location.lat);
            Assert.AreEqual(60, realEstate.Location.lng);
            Assert.AreEqual(5000, realEstate.Location.radius);

            Assert.AreEqual("RealEstate", realEstate.Name);

            Assert.AreEqual(3, realEstate.PoiData.Count);
        }
    }
}