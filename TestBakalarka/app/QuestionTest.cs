﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestBakalarka
{
    /// <summary>
    /// Souhrnný popis pro QuestionTest
    /// </summary>
    [TestClass]
    public class QuestionTest
    {
        public QuestionTest()
        {
            //
            // TODO: Zde přidejte konstruktory
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Získá nebo nastaví kontext testu, který přináší
        ///informace o a funkcionalita pro aktuální test.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Další atributy testu
        //
        // Při psaní testů můžete použít následující doplňkové atributy:
        //
        // Před spuštěním prvního testu ve třídě zavolejte funkci ClassInitialize
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Po dokončení všech testů ve třídě zavolejte funkci ClassCleanup
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Před provedením každého testu zavolejte funkci TestInitialize 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Po provedení každého testu zavolejte funkci TestCleanup
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {
            //
            // TODO: Zde přidejte testovací logiku
            //
        }
    }
}
