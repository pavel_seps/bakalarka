﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bakalarka.app;

namespace TestBakalarka
{
    /// <summary>
    /// Souhrnný popis pro ComparationTest
    /// </summary>
    [TestClass]
    public class ComparationTest
    {
        private Comparation coparation;

        public ComparationTest()
        {
            coparation = new Comparation();
        }

        [TestMethod]
        public void TestCompare()
        {
            List<ExternalData> realEstates = new List<ExternalData>();
            List<Question> questions = new List<Question>();

            Assert.IsNull(coparation.Compare(realEstates, questions, 0));
        }
    }
}
