namespace Bakalarka.MigrationsMapPoiModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MapPoiSets", "Address", c => c.String());
            AddColumn("dbo.MapPoiSets", "Icon", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MapPoiSets", "Icon");
            DropColumn("dbo.MapPoiSets", "Address");
        }
    }
}
