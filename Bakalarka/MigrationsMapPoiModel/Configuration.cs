namespace Bakalarka.MigrationsMapPoiModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class ConfigurationMapPoi : DbMigrationsConfiguration<Bakalarka.Models.MapPoiModel>
    {
        public ConfigurationMapPoi()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"MigrationsMapPoiModel";
        }

        protected override void Seed(Bakalarka.Models.MapPoiModel context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
