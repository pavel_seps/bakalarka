﻿using Bakalarka.app;
using Bakalarka.app.Communication;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Bakalarka.Controllers
{
    public class AnswerController : ApiController
    {
        // GET: api/Answer
        public async Task<string> Get()
        {
            Search search = HttpContext.Current.Session["search"] as Search;
            if (search == null)
                return new Transfer() { Status = "FAILED_SEARCH", Message = "Something went wrong" }.Stringify();

            Transfer<List<QuestionData>> t = new Transfer<List<QuestionData>>();

            try
            {
                t.Data = await search.GetAnswered();
            }
            catch
            {
                return new Transfer() { Status = "FAILED", Message = "Something went wrong" }.Stringify();
            }

            return t.Stringify();
        }

        // POST: api/Answer
        /// <summary>
        /// Set answers for questions
        /// </summary>
        /// <param name="request"></param>
        /// <returns>status</returns>
        public async Task<string> Post(HttpRequestMessage request)
        {
            Search search = HttpContext.Current.Session["search"] as Search;
            if (search == null)
                return new Transfer() { Status = "FAILED_SEARCH", Message = "Something went wrong" }.Stringify();

            string requestBody = request.Content.ReadAsStringAsync().Result;

            if (requestBody == null)
                return new Transfer() { Status = "FAILED_EMPTY_DATA", Message = "Something went wrong" }.Stringify();

            try
            {
                await search.SetAnswers(JsonConvert.DeserializeObject<List<AnswerData>>(requestBody));
            }
            catch
            {
                return new Transfer() { Status = "FAILED_INVALID_DATA", Message = "Something went wrong" }.Stringify();
            }

            return new Transfer().Stringify();
        }
    }
}
