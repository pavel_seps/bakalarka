﻿using Bakalarka.app.Communication;
using Bakalarka.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Bakalarka.Controllers
{
    public class CustomMapPoiController : ApiController
    {
        private MapPoiModel db = new MapPoiModel();

        // GET: api/CustomMapPoi
        public string Get()
        {
            Transfer<DbSet<MapPoiSet>> t = new Transfer<DbSet<MapPoiSet>>();
            t.Data = db.MapPoi;
            return t.Stringify();
        }

        // GET: api/CustomMapPoi/5
        public string Get(int id)
        {
            Transfer<MapPoiSet> t = new Transfer<MapPoiSet>();
            t.Data = db.MapPoi.Where(x => x.Id == id).First();
            
            if(t.Data == null)
            {
                return new Transfer() { Status = "FAILED", Message = "Something went wrong" }.Stringify();
            }
            return t.Stringify();
        }

        // POST: api/CustomMapPoi
        public string Post(HttpRequestMessage request)
        {
            string requestBody = request.Content.ReadAsStringAsync().Result;

            if (requestBody == null)
                return new Transfer() { Status = "FAILED_EMPTY_DATA", Message = "Something went wrong" }.Stringify();

            try
            {
                db.MapPoi.Add(JsonConvert.DeserializeObject<MapPoiSet>(requestBody));
                db.SaveChangesAsync();
            }
            catch
            {
                return new Transfer() { Status = "FAILED", Message = "Something went wrong" }.Stringify();
            }

            return new Transfer().Stringify();
        }

        // PUT: api/CustomMapPoi/5
        public string Put(int id, HttpRequestMessage request)
        {
            string requestBody = request.Content.ReadAsStringAsync().Result;

            if (requestBody == null)
                return new Transfer() { Status = "FAILED_EMPTY_DATA", Message = "Something went wrong" }.Stringify();

            MapPoiSet requestItem = JsonConvert.DeserializeObject<MapPoiSet>(requestBody);
            MapPoiSet dbItem = db.MapPoi.Where(x => x.Id == id).First();

            if (dbItem == null)
            {
                return new Transfer() { Status = "FAILED_NOT_FOUND", Message = "Something went wrong" }.Stringify();
            }

            try
            {
                dbItem.Name = requestItem.Name;
                dbItem.LatLocation = requestItem.LatLocation;
                dbItem.LngLocation = requestItem.LngLocation;
                dbItem.Category = requestItem.Category;
                dbItem.Address = requestItem.Address;
                dbItem.Icon = requestItem.Icon;
                db.SaveChangesAsync();
            }
            catch
            {
                return new Transfer() { Status = "FAILED", Message = "Something went wrong" }.Stringify();
            }

            return new Transfer().Stringify();
        }

        // DELETE: api/CustomMapPoi/5
        public string Delete(int id)
        {
            MapPoiSet item = db.MapPoi.Where(x => x.Id == id).First();

            if (item == null)
            {
                return new Transfer() { Status = "FAILED_NOT_FOUND", Message = "Something went wrong" }.Stringify();
            }

            try
            {
                db.MapPoi.Remove(item);
                db.SaveChangesAsync();
            }
            catch
            {
                return new Transfer() { Status = "FAILED", Message = "Something went wrong" }.Stringify();
            }

            return new Transfer().Stringify();
        }
    }
}
