﻿using Bakalarka.app;
using Bakalarka.app.Communication;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Bakalarka.Controllers
{
    public class SaveController : ApiController
    {

        // GET: api/Save
        public string Get()
        {
            Search search = HttpContext.Current.Session["search"] as Search;

            try
            {
                search.LoadStatus();
                return new Transfer().Stringify();
            }
            catch (Exception e)
            {
                if (e.Message == "Not done yet")
                {
                    return new Transfer() { Status = "FAILED_NOT_DONE", Message = e.Message }.Stringify();
                }

                return new Transfer() { Status = "FAILED", Message = e.Message }.Stringify();
            }
        }

        // GET: api/Save/hash
        public string Get(string id)
        {
            var session = HttpContext.Current.Session;

            session["search"] = new Search();

            Search search = session["search"] as Search;

            try
            {
                Task.Run(() =>
                {
                    search.Load(id);
                });
                return new Transfer().Stringify();
            }
            catch
            {
                return new Transfer() { Status = "FAILED_NOT_FOUND", Message = "Not found" }.Stringify();
            }
        }

        // POST: api/Save
        public async Task<string> Post(HttpRequestMessage request)
        {
            Search search = HttpContext.Current.Session["search"] as Search;
            if (search == null)
                return new Transfer() { Status = "FAILED_SEARCH", Message = "Something went wrong" }.Stringify();

            string requestBody = request.Content.ReadAsStringAsync().Result;
            string hash = "";
            try
            {
                string email = JsonConvert.DeserializeObject<SaveData>(requestBody).Email;
                hash = await search.Save(email);
            }
            catch (Exception e)
            {
                return new Transfer() { Status = "FAILED", Message = e.Message }.Stringify();
            }

            return new Transfer().Stringify();
        }

        // Delete: api/Save
        public string Put(string id, HttpRequestMessage request)
        {
            Search search = HttpContext.Current.Session["search"] as Search;
            if (search == null)
                return new Transfer() { Status = "FAILED_SEARCH", Message = "Something went wrong" }.Stringify();

            string requestBody = request.Content.ReadAsStringAsync().Result;
            string email = "";

            try
            {
                email = JsonConvert.DeserializeObject<SaveData>(requestBody).Email;
            }
            catch
            {
                return new Transfer() { Status = "FAILED_INVALID_DATA", Message = "Something went wrong" }.Stringify();
            }

            Save save = new Save();

            return save.Delete(id, email) == "done" ? new Transfer().Stringify() : new Transfer() { Status = "FAILED_NOT_FOUND", Message="Not found" }.Stringify();
        }
    }
}
