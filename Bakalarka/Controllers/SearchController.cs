﻿using Bakalarka.app;
using Bakalarka.app.Communication;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Bakalarka.Controllers
{
    public class SearchController : ApiController
    {
        // GET: api/Search
        /// <summary>
        /// Get result of searching
        /// </summary>
        /// <returns>List of realestates (JSON)</returns>
        public async Task<string> Get()
        {
            Search search = HttpContext.Current.Session["search"] as Search;
            if (search == null)
                return new Transfer() { Status = "FAILED_SEARCH", Message = "Something went wrong" }.Stringify();

            Transfer<List<ExternalData>> t = new Transfer<List<ExternalData>>();
            try
            {
                t.Data = await search.GetResult();
            }
            catch (Exception e)
            {
                if (e.Message == "Not done yet")
                {
                    return new Transfer() { Status = "FAILED_NOT_DONE", Message = e.Message }.Stringify();
                }
                else if (e.Message == "No data")
                {
                    return new Transfer() { Status = "FAILED_NOT_FOUND", Message = "Something went wrong" }.Stringify();
                }

                return new Transfer() { Status = "FAILED", Message = e.Message }.Stringify();
            }
            return t.Stringify();
        }

        public async Task<string> Get(string id)
        {
            Search search = HttpContext.Current.Session["search"] as Search;
            if (search == null)
                return new Transfer() { Status = "FAILED_SEARCH", Message = "Something went wrong" }.Stringify();

            Transfer<ExternalData> t = new Transfer<ExternalData>();

            try
            {
                t.Data = await search.GetResult(id);
            }
            catch (Exception e)
            {
                if(e.Message == "Not done yet")
                {
                    return new Transfer() { Status = "FAILED_NOT_DONE", Message = e.Message }.Stringify();
                }
                else if (e.Message == "No data")
                {
                    return new Transfer() { Status = "FAILED_NOT_FOUND", Message = "Something went wrong" }.Stringify();
                }
                return new Transfer() { Status = "FAILED", Message = e.Message }.Stringify();
            }
            return t.Stringify();
        }

        public string Post()
        {
            Search search = HttpContext.Current.Session["search"] as Search;
            
            try
            {
                search.SearchResult();
            }
            catch (Exception e)
            {
                if (e.Message == "No data")
                {
                    return new Transfer() { Status = "FAILED_NOT_FOUND", Message = "Something went wrong" }.Stringify();
                }
                return new Transfer() { Status = "FAILED_SEARCH", Message = e.Message }.Stringify();
            }

            return new Transfer().Stringify();
        }
    }
}
