﻿using Bakalarka.app;
using Bakalarka.app.Communication;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Bakalarka.Controllers
{
    public class FavoritePlaceController : ApiController
    {
        public async Task<string> Get()
        {
            Search search = HttpContext.Current.Session["search"] as Search;
            if (search == null)
                return new Transfer() { Status = "FAILED_SEARCH", Message = "Something went wrong" }.Stringify();

            Transfer<List<FavoritePlaceData>> t = new Transfer<List<FavoritePlaceData>>();
            t.Data = await search.GetFavoritePlaces();

            return t.Stringify();
        }

        public string Post(HttpRequestMessage request)
        {
            Search search = HttpContext.Current.Session["search"] as Search;
            if (search == null)
                return new Transfer() { Status = "FAILED_SEARCH", Message = "Something went wrong" }.Stringify();

            string requestBody = request.Content.ReadAsStringAsync().Result;

            if (requestBody == null)
                return new Transfer() { Status = "FAILED_EMPTY_DATA", Message = "Something went wrong" }.Stringify();

            try
            {
                search.SetFavoritePlaces(JsonConvert.DeserializeObject<List<FavoritePlaceData>>(requestBody));
            }
            catch
            {
                return new Transfer() { Status = "FAILED_INVALID_DATA", Message = "Something went wrong" }.Stringify();
            }

            return new Transfer().Stringify();
        }
    }
}
