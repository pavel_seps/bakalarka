﻿using Bakalarka.app;
using Bakalarka.app.Communication;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Bakalarka.Controllers
{
    public class LocationController : ApiController
    {
        // GET: api/Location
        public async Task<string> Get()
        {
            Search search = HttpContext.Current.Session["search"] as Search;
            if (search == null)
                return new Transfer() { Status = "FAILED_SEARCH", Message = "Something went wrong" }.Stringify();

            Transfer<LocationData> t = new Transfer<LocationData>();
            t.Data = await search.GetLocation();
            return t.Stringify();
        }

        // POST: api/Location
        /// <summary>
        /// Set location
        /// </summary>
        /// <param name="request"></param>
        /// <returns>status</returns>
        public string Post(HttpRequestMessage request)
        {

            var session = HttpContext.Current.Session;

            session["search"] = new Search();

            Search search = session["search"] as Search;
            if (search == null)
                return new Transfer() { Status = "FAILED_SEARCH", Message = "Something went wrong" }.Stringify();

            string requestBody = request.Content.ReadAsStringAsync().Result;

            if (requestBody == null)
                return new Transfer() { Status = "FAILED_EMPTY_DATA", Message = "Something went wrong" }.Stringify();

            try
            {
                search.SetLocation(JsonConvert.DeserializeObject<LocationData>(requestBody));
            }
            catch
            {
                return new Transfer() { Status = "FAILED_INVALID_DATA", Message = "Something went wrong" }.Stringify();
            }
            

            return new Transfer().Stringify();
        }
    }
}
