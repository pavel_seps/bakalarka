﻿using Bakalarka.app.Communication;
using Bakalarka.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Bakalarka.Controllers
{
    public class CustomRealEstateController : ApiController
    {
        private RealEstateModel db = new RealEstateModel();

        // GET: api/CustomRealEstate
        public string Get()
        {
            Transfer<DbSet<RealEstateSet>> t = new Transfer<DbSet<RealEstateSet>>();
            t.Data = db.RealEstate;
            return t.Stringify();
        }

        // GET: api/CustomRealEstate/5
        public string Get(int id)
        {
            Transfer<RealEstateSet> t = new Transfer<RealEstateSet>();
            t.Data = db.RealEstate.Where(x => x.Id == id).First();

            if (t.Data == null)
            {
                return new Transfer() { Status = "FAILED", Message = "Something went wrong" }.Stringify();
            }
            return t.Stringify();
        }

        // POST: api/CustomRealEstate
        public string Post(HttpRequestMessage request)
        {
            string requestBody = request.Content.ReadAsStringAsync().Result;

            if (requestBody == null)
                return new Transfer() { Status = "FAILED_EMPTY_DATA", Message = "Something went wrong" }.Stringify();
            
            try
            {
                db.RealEstate.Add(JsonConvert.DeserializeObject<RealEstateSet>(requestBody));
                db.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return new Transfer() { Status = "FAILED", Message = e.Message }.Stringify();
            }

            return new Transfer().Stringify();
        }

        // PUT: api/CustomRealEstate/5
        public string Put(int id, HttpRequestMessage request)
        {
            string requestBody = request.Content.ReadAsStringAsync().Result;

            if (requestBody == null)
                return new Transfer() { Status = "FAILED_EMPTY_DATA", Message = "Something went wrong" }.Stringify();

            RealEstateSet requestItem = JsonConvert.DeserializeObject<RealEstateSet>(requestBody);
            RealEstateSet dbItem = db.RealEstate.Where(x => x.Id == id).First();

            if(dbItem == null)
            {
                return new Transfer() { Status = "FAILED_NOT_FOUND", Message = "Something went wrong" }.Stringify();
            }
            
            try
            {
                dbItem.Name = requestItem.Name;
                dbItem.LatLocation = requestItem.LatLocation;
                dbItem.LngLocation = requestItem.LngLocation;
                dbItem.Address = requestItem.Address;
                dbItem.Info = requestItem.Info;
                dbItem.Price = requestItem.Price;
                dbItem.PriceNice = requestItem.PriceNice;
                dbItem.Type = requestItem.Type;
                db.SaveChangesAsync();
            }
            catch
            {
                return new Transfer() { Status = "FAILED", Message = "Something went wrong" }.Stringify();
            }

            return new Transfer().Stringify();
        }

        // DELETE: api/CustomRealEstate/5
        public string Delete(int id)
        {
            RealEstateSet item = db.RealEstate.Where(x => x.Id == id).First();
           
            if (item == null)
            {
                return new Transfer() { Status = "FAILED_NOT_FOUND", Message = "Something went wrong" }.Stringify();
            }

            try
            {
                db.RealEstate.Remove(item);
                db.SaveChangesAsync();
            }
            catch
            {
                return new Transfer() { Status = "FAILED", Message = "Something went wrong" }.Stringify();
            }

            return new Transfer().Stringify();
        }
    }
}
