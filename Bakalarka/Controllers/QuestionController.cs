﻿using Bakalarka.app;
using Bakalarka.app.Communication;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Bakalarka.Controllers
{
    public class QuestionController : ApiController
    {

        // GET: api/Question
        /// <summary>
        /// Get status of downloading questions
        /// </summary>
        /// <returns>0 - 1 double number of done %</returns>
        public async Task<string> Get()
        {
            Search search = HttpContext.Current.Session["search"] as Search;
            if (search == null)
                return new Transfer() { Status = "FAILED_SEARCH", Message = "Something went wrong" }.Stringify();

            Transfer<double> t = new Transfer<double>();
            t.Data = search.GetQuestionsStatus();

            return t.Stringify();
        }

        // GET: api/Question
        /// <summary>
        /// Get questions.
        /// Id must be "done"
        /// </summary>
        /// <returns>List of questions (JSON)</returns>
        public async Task<string> Get(string id)
        {
            Search search = HttpContext.Current.Session["search"] as Search;
            if (search == null)
                return new Transfer() { Status = "FAILED_SEARCH", Message = "Something went wrong" }.Stringify();

            if(id == "done")
            {
                Transfer<List<QuestionData>> t = new Transfer<List<QuestionData>>();
                t.Data = await search.GetQuestions();

                return t.Stringify();
            }
            else
            {
                return new Transfer() { Status = "FAILED_SEARCH", Message = "Something went wrong" }.Stringify();
            }
        }

        // Post: api/Question
        /// <summary>
        /// Start searching for questions
        /// </summary>
        /// <returns>Status</returns>
        public async Task<string> Post()
        {
            Search search = HttpContext.Current.Session["search"] as Search;
            if (search == null)
                return new Transfer() { Status = "FAILED_SEARCH", Message = "Something went wrong" }.Stringify();
            Transfer<List<QuestionData>> t = new Transfer<List<QuestionData>>();

            try
            {
                t.Data = await search.SearchQuestions();
                return t.Stringify();
            }
            catch
            {
                return new Transfer() { Status = "FAILED_SEARCH_QUESTIONS", Message = "Something went wrong" }.Stringify();
            }
        }
    }
}
