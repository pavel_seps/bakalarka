namespace Bakalarka.MigrationsSaveModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullAnswer : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AnswerSets", "Answer", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AnswerSets", "Answer", c => c.Int(nullable: false));
        }
    }
}
