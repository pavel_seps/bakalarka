namespace Bakalarka.MigrationsSaveModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFavoritPlaceName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FavoritPlaceSets", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FavoritPlaceSets", "Name");
        }
    }
}
