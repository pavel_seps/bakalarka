namespace Bakalarka.MigrationsSaveModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnswerSets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuestionId = c.String(),
                        Answer = c.Int(nullable: false),
                        Important = c.Boolean(nullable: false),
                        SaveSet_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SaveSets", t => t.SaveSet_Id)
                .Index(t => t.SaveSet_Id);
            
            CreateTable(
                "dbo.SaveSets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Hash = c.String(),
                        LatLocation = c.Double(nullable: false),
                        LngLocation = c.Double(nullable: false),
                        Radius = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnswerSets", "SaveSet_Id", "dbo.SaveSets");
            DropIndex("dbo.AnswerSets", new[] { "SaveSet_Id" });
            DropTable("dbo.SaveSets");
            DropTable("dbo.AnswerSets");
        }
    }
}
