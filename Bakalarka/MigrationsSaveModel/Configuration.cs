namespace Bakalarka.MigrationsSaveModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class ConfigurationSave : DbMigrationsConfiguration<Bakalarka.Models.SaveModel>
    {
        public ConfigurationSave()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"MigrationsSaveModel";
        }

        protected override void Seed(Bakalarka.Models.SaveModel context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
