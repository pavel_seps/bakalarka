namespace Bakalarka.MigrationsSaveModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFavoritPlace : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FavoritPlaceSets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                        LatLocation = c.Double(nullable: false),
                        LngLocation = c.Double(nullable: false),
                        SaveSet_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SaveSets", t => t.SaveSet_Id)
                .Index(t => t.SaveSet_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FavoritPlaceSets", "SaveSet_Id", "dbo.SaveSets");
            DropIndex("dbo.FavoritPlaceSets", new[] { "SaveSet_Id" });
            DropTable("dbo.FavoritPlaceSets");
        }
    }
}
