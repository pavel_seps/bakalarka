namespace Bakalarka.MigrationsRealEstateModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class ConfigurationRealEstate : DbMigrationsConfiguration<Bakalarka.Models.RealEstateModel>
    {
        public ConfigurationRealEstate()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"MigrationsRealEstateModel";
        }

        protected override void Seed(Bakalarka.Models.RealEstateModel context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
