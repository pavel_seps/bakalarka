namespace Bakalarka.MigrationsRealEstateModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RealEstateSets", "Info", c => c.String());
            AddColumn("dbo.RealEstateSets", "Address", c => c.String());
            AddColumn("dbo.RealEstateSets", "Type", c => c.String());
            AddColumn("dbo.RealEstateSets", "Price", c => c.Int(nullable: false));
            AddColumn("dbo.RealEstateSets", "PriceNice", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.RealEstateSets", "PriceNice");
            DropColumn("dbo.RealEstateSets", "Price");
            DropColumn("dbo.RealEstateSets", "Type");
            DropColumn("dbo.RealEstateSets", "Address");
            DropColumn("dbo.RealEstateSets", "Info");
        }
    }
}
