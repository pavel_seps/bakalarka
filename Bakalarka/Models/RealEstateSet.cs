﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Bakalarka.Models
{
    public class RealEstateSet
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Name { get; set; }

        public double LatLocation { get; set; }
        public double LngLocation { get; set; }

        public string Info { get; set; }
        public string Address { get; set; }
        public string Type { get; set; }

        public int Price { get; set; }
        public string PriceNice { get; set; }
    }
}