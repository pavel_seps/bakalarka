﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Bakalarka.Models
{
    public class AnswerSet
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string QuestionId { get; set; }

        public int? Answer { get; set; }

        public bool Important { get; set; }
    }
}