namespace Bakalarka.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class RealEstateModel : DbContext
    {
        public RealEstateModel()
            : base("name=DefaultConnection")
        {
        }
        
        public virtual DbSet<RealEstateSet> RealEstate { get; set; }
    }
}