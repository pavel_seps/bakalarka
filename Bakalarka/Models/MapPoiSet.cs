﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Bakalarka.Models
{
    public class MapPoiSet
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Category { get; set; }

        public double LatLocation { get; set; }
        public double LngLocation { get; set; }

        public string Address { get; set; }
        public string Icon { get; set; }

    }
}