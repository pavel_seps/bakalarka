namespace Bakalarka.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class SaveModel : DbContext
    {
        public SaveModel()
            : base("name=DefaultConnection")
        {
        }
        
        public virtual DbSet<SaveSet> Save { get; set; }
        public virtual DbSet<AnswerSet> Answer { get; set; }
        public virtual DbSet<FavoritPlaceSet> FavoritPLace { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SaveSet>()
                .HasMany(e => e.Answer);

            modelBuilder.Entity<SaveSet>()
                .HasMany(e => e.FavoritPlace);
        }
    }
}