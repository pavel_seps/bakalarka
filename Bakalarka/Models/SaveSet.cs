﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Bakalarka.Models
{
    public class SaveSet
    {
        public SaveSet()
        {
            Answer = new HashSet<AnswerSet>();
            FavoritPlace = new HashSet<FavoritPlaceSet>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Email { get; set; }
        public string Hash { get; set; }

        public virtual ICollection<AnswerSet> Answer { get; set; }
        public virtual ICollection<FavoritPlaceSet> FavoritPlace { get; set; }

        public double LatLocation { get; set; }
        public double LngLocation { get; set; }
        public int Radius { get; set; }

        public bool Active { get; set; }
    }
}