namespace Bakalarka.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class MapPoiModel : DbContext
    {
        public MapPoiModel()
            : base("name=DefaultConnection")
        {
        }
        
        public virtual DbSet<MapPoiSet> MapPoi { get; set; }
    }
}