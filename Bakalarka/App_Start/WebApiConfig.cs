﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Http.WebHost;
using System.Web.Routing;
using System.Web.SessionState;

namespace Bakalarka
{
    public class WebApiConfig : HttpApplication
    {


        public static void Register(RouteCollection routes)
        {
            var CustomRealEstateRoute = routes.MapHttpRoute(
                name: "CustomRealEstate",
                routeTemplate: "api/custom/real-estate/{id}",
                defaults: new { id = RouteParameter.Optional, controller = "CustomRealEstate" }
            );
            CustomRealEstateRoute.RouteHandler = new MyHttpControllerRouteHandler();

            var CustomMapPoiRoute = routes.MapHttpRoute(
                name: "CustomMapPoi",
                routeTemplate: "api/custom/map-poi/{id}",
                defaults: new { id = RouteParameter.Optional, controller = "CustomMapPoi" }
            );
            CustomMapPoiRoute.RouteHandler = new MyHttpControllerRouteHandler();

            var route = routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            route.RouteHandler = new MyHttpControllerRouteHandler();
        }
    }

    public class MyHttpControllerHandler : HttpControllerHandler, IRequiresSessionState
    {
        public MyHttpControllerHandler(RouteData routeData) : base(routeData)
        {
        }
    }
    public class MyHttpControllerRouteHandler : HttpControllerRouteHandler
    {
        protected override IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return new MyHttpControllerHandler(requestContext.RouteData);
        }
    }
}
