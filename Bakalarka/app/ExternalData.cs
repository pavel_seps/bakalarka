﻿using Bakalarka.app.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bakalarka.app
{
    /// <summary>
    /// Structure for data from external resources
    /// </summary>
    public abstract class ExternalData
    {
        private string name;

        public string Id { get; set; }
        public LocationData Location { get; set; }
        public List<PoiData> PoiData { get; set; } = new List<PoiData>();
        public string Name { get => name; set => name = value; }
    }
}