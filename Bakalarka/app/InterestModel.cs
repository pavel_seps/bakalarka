﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Bakalarka.app
{
    /// <summary>
    /// Get all interests
    /// </summary>
    public class InterestModel
    {

        /// <summary>
        /// Get interests from all resources
        /// </summary>
        /// <param name="realEstate">Real estate where to search around for interests</param>
        /// <returns>List of interests</returns>
        public ExternalData GetInterest(RealEstate realEstate)
        {
            var types = Assembly
                  .GetExecutingAssembly()
                  .GetTypes()
                  .Where(t => (t.Namespace == null ? "" : t.Namespace).Contains("InterestModels") && t.MemberType.ToString().Contains("TypeInfo"));

            realEstate.Interests.Clear();

            foreach (var t in types)
            {

                try
                {
                    Interest actualInterest = (Interest)Activator.CreateInstance(t, realEstate.Location);
                    realEstate.Interests.Add(actualInterest);
                }
                catch
                {
                    throw new Exception("Not used right interface in InterestModels");
                }
            }


            return realEstate;
        }
    }
}