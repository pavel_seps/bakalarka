﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bakalarka.app.Communication
{
    /// <summary>
    /// Data to transfer FavoritePlace
    /// </summary>
    public class FavoritePlaceData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public LocationData Location { get; set; }
        public string Type { get; set; }
        public int Rate { get; set; }
        public bool Hover { get; } = false;
    }
}