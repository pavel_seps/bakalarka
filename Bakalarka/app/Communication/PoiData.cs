﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bakalarka.app.Communication
{
    /// <summary>
    /// Data to transfer POI
    /// </summary>
    public class PoiData
    {
        public string Id { get; set; }
        public string Question { get; set; }
        public string QuestionShort { get; set; }
        public int Answer { get; set; }
        public LocationData Location { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Icon { get; set; }
        public int Distance { get; set; }
        public bool Hover { get; } = false;
    }
}