﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bakalarka.app.Communication
{
    /// <summary>
    /// Transfer loaded save
    /// </summary>
    public class ToSaveData
    {
        public List<AnswerData> Answers { get; set; }
        public LocationData Location { get; set; }
        public List<FavoritePlaceData> FavoritPlaces { get; set; }
    }
}