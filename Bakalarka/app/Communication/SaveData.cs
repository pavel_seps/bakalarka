﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bakalarka.app.Communication
{
    /// <summary>
    /// Transfer email for save
    /// </summary>
    public class SaveData
    {
        public string Email { get; set; }
    }
}