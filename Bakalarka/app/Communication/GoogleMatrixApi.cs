﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bakalarka.app.Communication
{
    /// <summary>
    /// Class for decompile JSON from Google Matrix API
    /// </summary>
    public class GoogleMatrixApi
    {
        public List<string> destination_addresses { get; set; }
        public List<string> origin_addresses { get; set; }
        public string status { get; set; }
        public List<RowMatrixApi> rows { get; set; }
    }

    public class RowMatrixApi
    {
        public List<ElementMatrixApi> elements { get; set; }
    }

    public class ElementMatrixApi
    {
        public string status { get; set; }
        public DurationMatrixApi duration { get; set; }
        public DurationTrafficMatrixApi duration_in_traffic { get; set; }
    }

    public class DurationMatrixApi
    {
        public double value { get; set; }
    }

    public class DurationTrafficMatrixApi
    {
        public double value { get; set; }
    }
}