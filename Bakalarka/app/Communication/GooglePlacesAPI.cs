﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bakalarka.app.Communication
{
    /// <summary>
    /// Class for decompile JSON from Google Places API
    /// </summary>
    public class GooglePacesAPI
    {
        public List<object> html_attributions { get; set; }
        public List<ResultPlacesAPI> results { get; set; }
        public string status { get; set; }
    }

    public class LocationPlacesAPI
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class GeometryPlacesAPI
    {
        public LocationPlacesAPI location { get; set; }
    }

    public class OpeningHoursPlacesAPI
    {
        public bool open_now { get; set; }
        public List<object> weekday_text { get; set; }
    }

    public class PhotoPlacesAPI
    {
        public int height { get; set; }
        public List<string> html_attributions { get; set; }
        public string photo_reference { get; set; }
        public int width { get; set; }
    }

    public class ResultPlacesAPI
    {
        public GeometryPlacesAPI geometry { get; set; }
        public string icon { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public OpeningHoursPlacesAPI opening_hours { get; set; }
        public List<PhotoPlacesAPI> photos { get; set; }
        public string place_id { get; set; }
        public double rating { get; set; }
        public string reference { get; set; }
        public string scope { get; set; }
        public List<string> types { get; set; }
        public string vicinity { get; set; }
    }
}