﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bakalarka.app.Communication
{
    /// <summary>
    /// Data to transfer answers
    /// </summary>
    public class AnswerData
    {
        public string Id { get; set; }
        public int? Answer { get; set; }
        public bool Important { get; set; }
    }
}