﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bakalarka.app.Communication
{
    /// <summary>
    /// Data to transfer location where is searching
    /// </summary>
    public class LocationData
    {
        public double lat { get; set; }
        public double lng { get; set; }
        public int radius { get; set; }

        public Tuple<double, double> getCoords()
        {
            return new Tuple<double, double>(lat, lng);
        }
    }
}