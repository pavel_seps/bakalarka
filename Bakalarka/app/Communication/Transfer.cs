﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bakalarka.app.Communication
{
    /// <summary>
    /// Main transfer class for sending messages via api to FE
    /// </summary>
    public class Transfer
    {
        public string Status { get; set; } = "OK";
        public string Message { get; set; } = "";

        /// <summary>
        /// Create json in string
        /// </summary>
        /// <returns>json in string</returns>
        public string Stringify()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    /// <summary>
    /// Main transfer class for sending messages via api to FE
    /// </summary>
    public class Transfer<T>
    {
        public string Status { get; set; } = "OK";
        public string Message { get; set; } = "";
        public T Data { get; set; }

        public Transfer() { }

        public Transfer(T data)
        {
            this.Data = data;
        }

        /// <summary>
        /// Create json in string
        /// </summary>
        /// <returns>json in string</returns>
        public string Stringify()
        {
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// Parse back json from string to class
        /// </summary>
        /// <param name="dataString">json in string</param>
        public void Parse(string dataString)
        {
            Transfer<T> t = JsonConvert.DeserializeObject<Transfer<T>>(dataString);
            this.Status = t.Status;
            this.Message = t.Message;
            this.Data = t.Data;
        }
    }
}