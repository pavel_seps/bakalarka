﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bakalarka.app.Communication
{
    /// <summary>
    /// Data to transfer question
    /// </summary>
    public class QuestionData
    {
        public string Id { get; set; }
        public string Question { get; set; }
        public string QuestionShort { get; set; }
        public List<Tuple<int, string>> Answers { get; set; }
        public int? Answer { get; set; } = null;
        public bool Important { get; set; }
    }
}