﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace Bakalarka.app
{
    /// <summary>
    /// Helper methods for project
    /// </summary>
    public class Helpers
    {

        /// <summary>
        /// Get distance between two points
        /// </summary>
        /// <param name="pos1">Lat,Lng first point</param>
        /// <param name="pos2">Lat,Lng second point</param>
        /// <returns>Distance in meters</returns>
        public static double GetDistance(Tuple<double, double> pos1, Tuple<double, double> pos2)
        {
            var sCoord = new GeoCoordinate(pos1.Item1, pos1.Item2);
            var eCoord = new GeoCoordinate(pos2.Item1, pos2.Item2);

            return sCoord.GetDistanceTo(eCoord);
        }

        private static double DegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        private static double RadianToDegree(double angle)
        {
            return angle * (180.0 / Math.PI);
        }

        public static Tuple<double, double> GetLocationByDistance(Tuple<double, double> point, double distance, int bearing)
        {
            double lat = DegreeToRadian(point.Item1);
            double lng = DegreeToRadian(point.Item2);
            double dR = distance / 6371000;
            double pointLat = Math.Asin(Math.Sin(lat) * Math.Cos(dR) +
                    Math.Cos(lat) * Math.Sin(dR) * Math.Cos(DegreeToRadian(bearing)));
            double pointLng = lng + Math.Atan2(Math.Sin(DegreeToRadian(bearing)) * Math.Sin(dR) * Math.Cos(lat),
                                     Math.Cos(dR) - Math.Sin(lat) * Math.Sin(pointLat));


            return new Tuple<double, double>(RadianToDegree(pointLat), RadianToDegree(pointLng)); 
        }

        /// <summary>
        /// Get request to api server
        /// </summary>
        /// <param name="uri">Url to api</param>
        /// <returns>json in string</returns>
        public static string Get(string uri)
        {
            HttpWebRequest request;
            try
            {
                request = (HttpWebRequest)WebRequest.Create(uri);
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                request.Timeout = 8000;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// Get async request to api server
        /// </summary>
        /// <param name="uri">Url to api</param>
        /// <returns>json in string</returns>
        public static async Task<string> GetAsync(string uri)
        {
            HttpWebRequest request;
            try
            {
                request = (HttpWebRequest)WebRequest.Create(uri);
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                using (HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    return await reader.ReadToEndAsync();
                }
            }
            catch
            {
                return "";
            }
        }
    }
}