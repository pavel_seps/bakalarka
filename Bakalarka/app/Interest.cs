﻿using Bakalarka.app.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bakalarka.app
{
    /// <summary>
    /// Interest class
    /// </summary>
    public abstract class Interest
    {
        public Interest(LocationData location)
        {
            this.location = location;
            this.Id = SetId();
            this.Name = SetName();
            this.Value = SetValue();
        }


        protected LocationData location;

        private string _Id;
        public string Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                this._Id = value;
            }
        }
        protected abstract string SetId();

        private string _Value;
        public string Value
        {
            get
            {
                return this._Value;
            }
            set
            {
                this._Value = value;
            }
        }
        protected abstract string SetValue();

        private string _Name;
        public string Name
        {
            get
            {
                return this._Name;
            }
            set
            {
                this._Name = value;
            }
        }
        protected abstract string SetName();
    }
}