﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;

namespace Bakalarka.app
{
    /// <summary>
    /// Send email
    /// </summary>
    public class Email
    {
        /// <summary>
        /// Send saved hash to customer
        /// </summary>
        /// <param name="hash">hash of saved list</param>
        /// <param name="to">email of customer</param>
        public void SendSavedHash(string hash, string to)
        {
            var config = ConfigurationManager.AppSettings;

            SmtpClient client = new SmtpClient(config["SMTPserver"], Int32.Parse(config["SMTPport"]))
            {
                Credentials = new NetworkCredential(config["SMTPuser"], config["SMTPpassword"]),
                EnableSsl = true,
            };

            string template = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\Email\saved.html");

            MailDefinition md = new MailDefinition();
            md.From = config["SMTPfrom"];
            md.IsBodyHtml = true;
            md.Subject = "Uložení - Individualizované hodnocení nemovitostí";

            ListDictionary replacements = new ListDictionary();
            replacements.Add("{{urlLoad}}", config["website"]+ config["loadSaveSubpage"] + "/" +hash);
            replacements.Add("{{urlRemove}}", config["website"] + config["removeSaveSubpage"] + "/" + hash);


            MailMessage mail = md.CreateMailMessage(to, replacements, template, new System.Web.UI.Control());


            client.Send(mail);
        }
    }
}