﻿using Bakalarka.app.Communication;
using Bakalarka.app.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Bakalarka.app.MapModels
{
    public class GoogleSchool : DataConnection
    {
        public List<ExternalData> GetData(LocationData location)
        {
            List<ExternalData> returnCollection = new List<ExternalData>();
            string key = ConfigurationManager.AppSettings["googleApiKey"];

            string lat = location.lat.ToString().Replace(',', '.');
            string lng = location.lng.ToString().Replace(',', '.');

            string url = $"https://maps.googleapis.com/maps/api/place/nearbysearch/json?key={key}&language=cs&location={lat},{lng}&radius={location.radius}&type=school";
            string result = Helpers.Get(url);
            GooglePacesAPI resultClass = JsonConvert.DeserializeObject<GooglePacesAPI>(result);

            foreach (ResultPlacesAPI res in resultClass.results)
            {
                returnCollection.Add(new MapPOI()
                {
                    Name = res.name,
                    Location = new LocationData() { lat = res.geometry.location.lat, lng = res.geometry.location.lng },
                    Address = res.vicinity,
                    Icon = res.icon,
                    Id = res.id
                });
            }

            return returnCollection;
        }
    }
}