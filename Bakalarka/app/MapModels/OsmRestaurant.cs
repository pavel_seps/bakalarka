﻿using Bakalarka.app.Communication;
using Bakalarka.app.Interface;
using Bakalarka.app.MapModels.Questions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bakalarka.app.MapModels
{
    public class OsmRestaurant : DataConnection
    {
        public List<ExternalData> GetData(LocationData location)
        {
            List<ExternalData> returnCollection = new List<ExternalData>();

            string lat = location.lat.ToString().Replace(',', '.');
            string lng = location.lng.ToString().Replace(',', '.');

            string url = $"<osm-script output=\"json\"><query type=\"node\"><around lat=\"{lat}\" lon=\"{lng}\" radius=\"{location.radius}\" /><has-kv k=\"amenity\" v=\"restaurant\" /></query><print e = \"\" from = \"_\" geometry = \"skeleton\" limit = \"\" mode = \"body\" n = \"\" order = \"id\" s = \"\" w = \"\" /></osm-script>";
            url = $"http://overpass-api.de/api/interpreter?data={url}";
            string result = Helpers.Get(url);
            if (result == "")
            {
                return returnCollection;
            }
            var resultClass = JObject.Parse(result);

            foreach (var res in resultClass["elements"])
            {
                returnCollection.Add(new MapPOI()
                {
                    Name = (string)res["tags"]["name"] ?? "",
                    Location = new LocationData() { lat = (double)res["lat"], lng = (double)res["lon"] },
                    Address = (string)res["tags"]["is_in"] ?? "",
                    Icon = "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
                    Id = (string)res["id"]
                });
            }

            return returnCollection;
        }
    }
}