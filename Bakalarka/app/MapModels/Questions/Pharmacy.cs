﻿using Bakalarka.app;
using Bakalarka.app.Communication;
using Bakalarka.app.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bakalarka.app.MapModels.Questions
{
    public class Pharmacy : Question
    {
        public Pharmacy(LocationData location) : base(location)
        {
            this.GetExternalData(() =>
            {
                List<ExternalData> data = new List<ExternalData>();
                data.AddRange(new GooglePharmacy().GetData(location));
                data.AddRange(new OsmPharmacy().GetData(location));
                data.AddRange(new CustomDb("pharmacy").GetData(location));
                return data;
            });

        }

        protected override List<Tuple<int, string>> SetAnswers()
        {
            List<Tuple<int, string>> answers = new List<Tuple<int, string>>();
            answers.Add(new Tuple<int, string>(1, "Ne"));
            answers.Add(new Tuple<int, string>(2, "Nemusí být"));
            answers.Add(new Tuple<int, string>(3, "Možná"));
            answers.Add(new Tuple<int, string>(4, "Ano"));
            return answers;
        }

        protected override string SetId()
        {
            return "pharmacyQuestion";
        }

        protected override string SetQuestionText()
        {
            return "Chcete blízko lekárnu?";
        }

        protected override string SetQuestionTextShort()
        {
            return "Lékárna";
        }
    }
}