﻿using Bakalarka.app;
using Bakalarka.app.Communication;
using Bakalarka.app.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bakalarka.app.MapModels.Questions
{
    public class PostOffice : Question
    {
        public PostOffice(LocationData location) : base(location)
        {
            this.GetExternalData(() =>
            {
                List<ExternalData> data = new List<ExternalData>();
                data.AddRange(new GooglePostOffice().GetData(location));
                data.AddRange(new OsmPostOffice().GetData(location));
                data.AddRange(new CustomDb("postOffice").GetData(location));
                return data;
            });

        }

        protected override List<Tuple<int, string>> SetAnswers()
        {
            List<Tuple<int, string>> answers = new List<Tuple<int, string>>();
            answers.Add(new Tuple<int, string>(1, "Ne"));
            answers.Add(new Tuple<int, string>(2, "Ano"));
            return answers;
        }

        protected override string SetId()
        {
            return "postOfficeQuestion";
        }

        protected override string SetQuestionText()
        {
            return "Potřebujete často chodit na poštu?";
        }

        protected override string SetQuestionTextShort()
        {
            return "Pošta";
        }
    }
}