﻿using Bakalarka.app;
using Bakalarka.app.Communication;
using Bakalarka.app.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bakalarka.app.MapModels.Questions
{
    public class School : Question
    {
        public School(LocationData location) : base(location)
        {
            this.GetExternalData(() =>
            {
                List<ExternalData> data = new List<ExternalData>();
                data.AddRange(new GoogleSchool().GetData(location));
                data.AddRange(new OsmSchool().GetData(location));
                data.AddRange(new CustomDb("school").GetData(location));
                return data;
            });

        }

        protected override List<Tuple<int, string>> SetAnswers()
        {
            List<Tuple<int, string>> answers = new List<Tuple<int, string>>();
            answers.Add(new Tuple<int, string>(1, "Ne"));
            answers.Add(new Tuple<int, string>(2, "Nemusí být"));
            answers.Add(new Tuple<int, string>(3, "Možná"));
            answers.Add(new Tuple<int, string>(4, "Ano"));
            return answers;
        }

        protected override string SetId()
        {
            return "schoolQuestion";
        }

        protected override string SetQuestionText()
        {
            return "Přejete si blízko školu?";
        }

        protected override string SetQuestionTextShort()
        {
            return "Škola";
        }
    }
}