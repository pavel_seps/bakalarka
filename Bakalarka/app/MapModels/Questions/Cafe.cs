﻿using Bakalarka.app;
using Bakalarka.app.Communication;
using Bakalarka.app.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bakalarka.app.MapModels.Questions
{
    public class Cafe : Question
    {
        public Cafe(LocationData location) : base(location)
        {
            this.GetExternalData(() =>
            {
                List<ExternalData> data = new List<ExternalData>();
                data.AddRange(new GoogleCafe().GetData(location));
                data.AddRange(new OsmCafe().GetData(location));
                data.AddRange(new CustomDb("cafe").GetData(location));
                return data;
            });

        }

        protected override List<Tuple<int, string>> SetAnswers()
        {
            List<Tuple<int, string>> answers = new List<Tuple<int, string>>();
            answers.Add(new Tuple<int, string>(1, "Ne"));
            answers.Add(new Tuple<int, string>(2, "Občas"));
            answers.Add(new Tuple<int, string>(3, "Ano"));
            return answers;
        }

        protected override string SetId()
        {
            return "cafeQuestion";
        }

        protected override string SetQuestionText()
        {
            return "Chodíte rádi do kavárny?";
        }

        protected override string SetQuestionTextShort()
        {
            return "Kavárna";
        }
    }
}