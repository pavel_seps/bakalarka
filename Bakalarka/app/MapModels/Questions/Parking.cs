﻿using Bakalarka.app;
using Bakalarka.app.Communication;
using Bakalarka.app.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bakalarka.app.MapModels.Questions
{
    public class Parking : Question
    {
        public Parking(LocationData location) : base(location)
        {
            this.GetExternalData(() =>
            {
                List<ExternalData> data = new List<ExternalData>();
                data.AddRange(new GoogleParking().GetData(location));
                data.AddRange(new OsmParking().GetData(location));
                data.AddRange(new OsmParkingEntrance().GetData(location));
                data.AddRange(new CustomDb("parking").GetData(location));
                return data;
            });

        }

        protected override List<Tuple<int, string>> SetAnswers()
        {
            List<Tuple<int, string>> answers = new List<Tuple<int, string>>();
            answers.Add(new Tuple<int, string>(1, "Ne"));
            answers.Add(new Tuple<int, string>(2, "Ano"));
            return answers;
        }

        protected override string SetId()
        {
            return "parkingQuestion";
        }

        protected override string SetQuestionText()
        {
            return "Potřebujete poblíž parkoviště?";
        }

        protected override string SetQuestionTextShort()
        {
            return "Parkoviště";
        }
    }
}