﻿using Bakalarka.app;
using Bakalarka.app.Communication;
using Bakalarka.app.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bakalarka.app.MapModels.Questions
{
    public class Park : Question
    {
        public Park(LocationData location) : base(location)
        {
            this.GetExternalData(() =>
            {
                List<ExternalData> data = new List<ExternalData>();
                data.AddRange(new GooglePark().GetData(location));
                data.AddRange(new OsmPark().GetData(location));
                data.AddRange(new OsmPlayground().GetData(location));
                data.AddRange(new CustomDb("park").GetData(location));
                return data;
            });

        }

        protected override List<Tuple<int, string>> SetAnswers()
        {
            List<Tuple<int, string>> answers = new List<Tuple<int, string>>();
            answers.Add(new Tuple<int, string>(1, "Ne"));
            answers.Add(new Tuple<int, string>(2, "Občas"));
            answers.Add(new Tuple<int, string>(3, "Ano"));
            return answers;
        }

        protected override string SetId()
        {
            return "parkQuestion";
        }

        protected override string SetQuestionText()
        {
            return "Chodite se procházet do parku?";
        }

        protected override string SetQuestionTextShort()
        {
            return "Park";
        }
    }
}