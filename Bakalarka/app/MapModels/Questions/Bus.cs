﻿using Bakalarka.app;
using Bakalarka.app.Communication;
using Bakalarka.app.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bakalarka.app.MapModels.Questions
{
    public class Bus : Question
    {
        public Bus(LocationData location) : base(location)
        {
            this.GetExternalData(() =>
            {
                List<ExternalData> data = new List<ExternalData>();
                data.AddRange(new GoogleBus().GetData(location));
                data.AddRange(new OsmBus().GetData(location));
                data.AddRange(new CustomDb("bus").GetData(location));
                return data;
            });

        }

        protected override List<Tuple<int, string>> SetAnswers()
        {
            List<Tuple<int, string>> answers = new List<Tuple<int, string>>();
            answers.Add(new Tuple<int, string>(1, "Ne"));
            answers.Add(new Tuple<int, string>(2, "Jednou za čas"));
            answers.Add(new Tuple<int, string>(3, "Ano"));
            return answers;
        }

        protected override string SetId()
        {
            return "busQuestion";
        }

        protected override string SetQuestionText()
        {
            return "Jezdite autobusem?";
        }

        protected override string SetQuestionTextShort()
        {
            return "Zastávka";
        }
    }
}