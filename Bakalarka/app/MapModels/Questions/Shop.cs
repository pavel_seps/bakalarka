﻿using Bakalarka.app;
using Bakalarka.app.Communication;
using Bakalarka.app.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bakalarka.app.MapModels.Questions
{
    public class Shop : Question
    {
        public Shop(LocationData location) : base(location)
        {
            this.GetExternalData(() =>
            {
                List<ExternalData> data = new List<ExternalData>();
                data.AddRange(new GoogleStore().GetData(location));
                data.AddRange(new GoogleSupermarket().GetData(location));
                data.AddRange(new OsmMall().GetData(location));
                data.AddRange(new OsmSupermarket().GetData(location));
                data.AddRange(new CustomDb("shop").GetData(location));
                return data;
            });
        }

        protected override List<Tuple<int, string>> SetAnswers()
        {
            List<Tuple<int, string>> answers = new List<Tuple<int, string>>();
            answers.Add(new Tuple<int, string>(1, "Ne"));
            answers.Add(new Tuple<int, string>(2, "Občas"));
            answers.Add(new Tuple<int, string>(3, "Ano"));
            return answers;
        }

        protected override string SetId()
        {
            return "shopQuestion";
        }

        protected override string SetQuestionText()
        {
            return "Chodíte často do obchodu?";
        }

        protected override string SetQuestionTextShort()
        {
            return "Obchod";
        }
    }
}