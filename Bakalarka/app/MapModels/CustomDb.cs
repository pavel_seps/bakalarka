﻿using Bakalarka.app.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bakalarka.app.Communication;
using Bakalarka.Models;

namespace Bakalarka.app.MapModels
{
    public class CustomDb : DataConnection
    {
        private MapPoiModel db = new MapPoiModel();
        private string category = "";

        public CustomDb(string cat)
        {
            category = cat;
        }

        public List<ExternalData> GetData(LocationData location)
        {
            List<ExternalData> poiCollection = new List<ExternalData>();
            int index = 0;
            foreach (MapPoiSet poi in db.MapPoi)
            {
                if (poi.Category == category && Helpers.GetDistance(location.getCoords(), new Tuple<double, double>(poi.LatLocation, poi.LngLocation)) < location.radius)
                {
                    poiCollection.Add(new MapPOI()
                    {
                        Name = poi.Name,
                        Location = new LocationData() { lat = poi.LatLocation, lng = poi.LngLocation },
                        Address = poi.Address,
                        Icon = poi.Icon,
                        Id = "custom-"+ index++
                    });
                }
            }

            return poiCollection;
        }
    }
}