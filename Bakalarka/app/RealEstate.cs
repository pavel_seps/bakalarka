﻿using Bakalarka.app.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bakalarka.app
{
    /// <summary>
    /// Real estate class
    /// </summary>
    public class RealEstate : ExternalData
    {
        public int Percentage { get; set; }
        public int Price { get; set; }
        public string PriceNice { get; set; }
        public string PriceUnit { get; set; }
        public string Type { get; set; }
        public string InfoText { get; set; }
        public string Address { get; set; }


        public List<Interest> Interests { get; set; }
        public List<FavoritePlaceData> FavoritePlace { get; set; }

        public RealEstate()
        {
            this.Interests = new List<Interest>();
            this.FavoritePlace = new List<FavoritePlaceData>();
        }
    }
}