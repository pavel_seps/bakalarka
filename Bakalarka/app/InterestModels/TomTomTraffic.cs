﻿using Bakalarka.app.Communication;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Bakalarka.app.InterestModels
{
    public class TomTomTraffic : Interest
    {
        public TomTomTraffic(LocationData location) : base(location)
        {
        }

        protected override string SetId()
        {
            return "actualTomTomTrafficFlow";
        }

        protected override string SetName()
        {
            return "Aktuální doprava";
        }

        protected override string SetValue()
        {
            string key = ConfigurationManager.AppSettings["tomtomApiKey"];
            string lat = location.lat.ToString().Replace(',', '.');
            string lng = location.lng.ToString().Replace(',', '.');
            string url = $"https://api.tomtom.com/traffic/services/4/flowSegmentData/absolute/10/json?key={key}&point={lat},{lng}";
            string result = "";
            try
            {
                result = Helpers.Get(url);
            }
            catch
            {
                return "-1";
            }

            
            var resultParsed = JObject.Parse(result);

            double confidence = (double)resultParsed["flowSegmentData"]["confidence"];

            if(confidence < 0.4)
            {
                return "-1";
            }

            double currentSpeed = (double)resultParsed["flowSegmentData"]["currentSpeed"];
            double freeFlow = (double)resultParsed["flowSegmentData"]["freeFlowSpeed"];
            double ratio = freeFlow / currentSpeed;
            ratio = ratio > 1 ? 1 : ratio;

            return ratio.ToString();
        }
    }
}