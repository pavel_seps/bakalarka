﻿using Bakalarka.app.Communication;
using Bakalarka.app.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Bakalarka.app
{
    /// <summary>
    /// Unite all resources for real estates
    /// </summary>
    public class RealEstateDataModel
    {

        /// <summary>
        /// Get async Task with all reale states from all resources
        /// </summary>
        /// <param name="location">Location where to search</param>
        /// <returns>Task with list of all real estates</returns>
        public List<ExternalData> GetRealEstates(LocationData location)
        {
            List<Task<List<ExternalData>>> resultCollection = new List<Task<List<ExternalData>>>();

            var types =  Assembly
                .GetExecutingAssembly()
                .GetTypes()
                .Where(t => (t.Namespace == null ? "" : t.Namespace).Contains("RealEstateModels") && t.MemberType.ToString().Contains("TypeInfo"));

            foreach (var t in types)
            {
                try
                {
                    DataConnection connection = (DataConnection)Activator.CreateInstance(t);

                    Task<List<ExternalData>> actualTask = new Task<List<ExternalData>>(() =>
                    {
                        return connection.GetData(location);
                    });
                    actualTask.Start();
                    resultCollection.Add(actualTask);
                }
                catch
                {
                    throw new Exception("Not used right interface in RealEsateModels");
                }
            }
        
            
            List<ExternalData>[] allData = Task.WhenAll(resultCollection).GetAwaiter().GetResult();
            List<ExternalData> listData = new List<ExternalData>();

            foreach (List<ExternalData> list in allData)
            {
                listData.AddRange(list);
            }

            return listData.GroupBy(x => new { x.Location, x.Name}).Select(x => x.First()).ToList();
        }
    }
}