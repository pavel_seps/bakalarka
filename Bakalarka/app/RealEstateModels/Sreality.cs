﻿using Bakalarka.app.Communication;
using Bakalarka.app.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bakalarka.app.RealEstateModels
{
    public class Sreality : DataConnection
    {
        public List<ExternalData> GetData(LocationData location)
        {
            return GetAsync(location).Result;
        }

        private async Task<List<ExternalData>> GetAsync(LocationData location)
        {
            List<ExternalData> realitis = new List<ExternalData>();
            List<Task<string>> realitisId = new List<Task<string>>();
            Tuple<List<string>,List<Task<string>>> realitisDetail = new Tuple<List<string>, List<Task<string>>>(new List<string>(), new List<Task<string>>());
            List<ClusterSreality> realitisIdList = new List<ClusterSreality>();

            string leftBottomlat = Helpers.GetLocationByDistance(location.getCoords(), location.radius / 2, 180).Item1.ToString().Replace(',', '.');
            string leftBottomlng = Helpers.GetLocationByDistance(location.getCoords(), location.radius / 2, 270).Item2.ToString().Replace(',', '.');
            string rightToplat = Helpers.GetLocationByDistance(location.getCoords(), location.radius / 2, 0).Item1.ToString().Replace(',', '.');
            string rightToplng = Helpers.GetLocationByDistance(location.getCoords(), location.radius / 2, 90).Item2.ToString().Replace(',', '.');
            realitisId.Add(Helpers.GetAsync($"http://www.sreality.cz/api/cs/v2/clusters?leftBottomBounding={leftBottomlng}%7C{leftBottomlat}&rightTopBounding={rightToplng}%7C{rightToplat}&zoom=18&category_main_cb=1"));
            realitisId.Add(Helpers.GetAsync($"http://www.sreality.cz/api/cs/v2/clusters?leftBottomBounding={leftBottomlng}%7C{leftBottomlat}&rightTopBounding={rightToplng}%7C{rightToplat}&zoom=18&category_main_cb=2"));

            foreach (string idList in await Task.WhenAll(realitisId))
            {
                realitisIdList.AddRange(JsonConvert.DeserializeObject<DataSreality>(idList).clusters);
            }

            foreach (ClusterSreality cluster in realitisIdList)
            {
                if (Helpers.GetDistance(new Tuple<double, double>(cluster.center.lat, cluster.center.lon), location.getCoords()) <= location.radius)
                {
                    foreach (string id in cluster.estate_ids)
                    {
                        realitisDetail.Item1.Add(id);
                        realitisDetail.Item2.Add(Helpers.GetAsync($"http://www.sreality.cz/api/cs/v2/estates/{id}"));
                    }
                }
            }

            int index = 0;
            foreach(string requestDataDetail in await Task.WhenAll(realitisDetail.Item2))
            {
                if(requestDataDetail != "")
                {
                    DataSrealityDetail dataDetail = JsonConvert.DeserializeObject<DataSrealityDetail>(requestDataDetail);

                    realitis.Add(new RealEstate()
                    {
                        Id = "steality-" + realitisDetail.Item1.ElementAt(index++),
                        Name = dataDetail.name.value,
                        Location = new LocationData() { lat = dataDetail.map.lat, lng = dataDetail.map.lon },
                        Type = dataDetail.seo.category_type_cb == 1 ? "sale" : "rent",
                        Price = dataDetail.price_czk.value_raw,
                        PriceNice = dataDetail.price_czk.value,
                        PriceUnit = dataDetail.price_czk.unit,
                        Address = dataDetail.locality.value,
                        InfoText = dataDetail.meta_description
                    });
                }
            }

            return realitis;
        }
    }
}

class DataSreality
{
    public List<ClusterSreality> clusters { get; set; }
}

class ClusterSreality
{
    public List<string> estate_ids { get; set; }
    public LocationSreality center { get; set; }

}

class DataSrealityDetail
{
    public string meta_description { get; set; }
    public NameValueSrealityDetail name { get; set; }
    public LocationSreality map { get; set; }
    public NameValueSrealityDetail locality { get; set; }
    public NameValueSrealityDetail text { get; set; }
    public PriceSrealityDetail price_czk { get; set; }
    public SeoSrealityDetail seo { get; set; }
    //public ItemSrealityDetail items { get; set; } //TODO get items from request
}

class LocationSreality
{
    public double lat { get; set; }
    public double lon { get; set; }
}

class NameValueSrealityDetail
{
    public string name { get; set; }
    public string value { get; set; }
}

class PriceSrealityDetail
{
    public string unit { get; set; }
    public string value { get; set; }
    public int value_raw { get; set; }
}

class ItemSrealityDetail
{
    public string type { get; set; }
    public string name { get; set; }
    public string value { get; set; }
}

class SeoSrealityDetail
{
    public int category_type_cb { get; set; }
}