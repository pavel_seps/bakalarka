﻿using Bakalarka.app.Communication;
using Bakalarka.app.Interface;
using Bakalarka.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bakalarka.app.RealEstateModels
{
    public class CustomDbRealEstates : DataConnection
    {
        private RealEstateModel db = new RealEstateModel();

        public List<ExternalData> GetData(LocationData location)
        {
            List<ExternalData> reCollection = new List<ExternalData>();
            int index = 0;
            foreach (RealEstateSet re in db.RealEstate)
            {
                if(Helpers.GetDistance(location.getCoords(), new Tuple<double, double>(re.LatLocation, re.LngLocation)) < location.radius)
                {
                    reCollection.Add(new RealEstate()
                    {
                        Name = re.Name,
                        Location = new LocationData() { lat = re.LatLocation, lng = re.LngLocation},
                        Id = "custom-"+index++,
                        Address = re.Address,
                        InfoText = re.Info,
                        Price = re.Price,
                        PriceNice = re.PriceNice,
                        Type = re.Type,
                        PriceUnit = re.Type == "sale" ? "" : "za měsíc"
                    });
                }
            }

            return reCollection;
        }
    }
}