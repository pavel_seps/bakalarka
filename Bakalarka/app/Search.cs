﻿using Bakalarka.app.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bakalarka.app
{
    /// <summary>
    /// Class for application where is main logic
    /// </summary>
    public class Search
    {
        private int status = 0;
        Dictionary<int, string> statusLabels = new Dictionary<int, string>() {
            { 0, "notInitialized" },
            { 1, "init" },
            { 2, "setLocation" },
            { 3, "gotQuestions" },
            { 4, "setAnswers" },
            { 5, "setFavoritePlace" },
            { 6, "searched" },
            { 7, "gotResult" }
        };

        private Task<LocationData> searchLocation;
        private Task<List<ExternalData>> result;
        private Task<List<ExternalData>> realEstates;
        private Task<List<FavoritePlaceData>> favoritePlaces;
        private List<Task<Question>> questionsList;
        private Task<List<Question>> answers;
        private Task<List<AnswerData>> answerData;
        private MapDataModel questions;
        private Rating rating;
        private RealEstateDataModel realEstateDataModel;
        private Comparation compare;
        private Save save;
        private InterestModel interestModel;

        /// <summary>
        /// Init all components
        /// </summary>
        public Search()
        {
            questions = new MapDataModel();
            rating = new Rating();
            realEstateDataModel = new RealEstateDataModel();
            compare = new Comparation();
            save = new Save();
            interestModel = new InterestModel();
            this.status = 1;
        }

        /// <summary>
        /// Set location for searching
        /// </summary>
        /// <param name="data">Location where to search</param>
        public void SetLocation(LocationData data)
        {
            if(this.status < 1)
            {
                throw new Exception("Not properly order of methods call");
            }

            this.searchLocation = Task.Run(() => { return data; });

            this.status = 2;
        }

        public async Task<LocationData> GetLocation()
        {
            return await this.searchLocation;
        }

        /// <summary>
        /// Rate and compare real estates with questions.
        /// Set result
        /// </summary>
        public async Task SearchResult()
        {
            if (this.status < 5)
            {
                throw new Exception("Not properly order of methods call");
            }
            
            if (
                this.realEstates.Status == TaskStatus.Faulted ||
                (await this.realEstates) == null ||
                (await this.realEstates).Count < 1 ||
                this.answers.Status == TaskStatus.Faulted ||
                (await this.answers) == null ||
                (await this.answers).Count < 1
                )
            {
                throw new Exception("No data");
            }

            this.result = Task.Run(async () => 
            {
                return compare.Compare(await this.realEstates, await this.answers, await this.favoritePlaces == null ? 0 : (await this.favoritePlaces).Count);
            });
            this.status = 6;
        }


        public async Task<List<QuestionData>> SearchQuestions()
        {
            if (this.status < 2)
            {
                throw new Exception("Not properly order of methods call");
            }

            this.questionsList = questions.GetQuestions(await this.searchLocation);

            List<QuestionData> returnValue = new List<QuestionData>();
            QuestionData data = null;

            foreach (Question question in await Task.WhenAll(this.questionsList)){
                data = new QuestionData()
                {
                    Id = question.Id,
                    Answers = question.Answers,
                    Question = question.QuestionText,
                    Answer = null,
                    QuestionShort = question.QuestionTextShort
                };
                returnValue.Add(data);
            }

            this.realEstates = Task.Run(async () =>
            {
                List<ExternalData> localRealEstates = realEstateDataModel.GetRealEstates(await this.searchLocation);
                return rating.RateQuestions(localRealEstates, (await Task.WhenAll(this.questionsList)).Where(x => x != null).ToList());
            });

            this.status = 3;

            return returnValue;
        }

        /// <summary>
        /// Get questions
        /// </summary>
        /// <returns>List of light question data</returns>
        public async Task<List<QuestionData>> GetQuestions()
        {
            if (this.status < 2)
            {
                throw new Exception("Not properly order of methods call");
            }
            
            List<QuestionData> returnValue = new List<QuestionData>();
            QuestionData data = null;
            
            foreach (Question question in (await Task.WhenAll(this.questionsList)).Where(x => x != null).ToList())
            {
                data = new QuestionData() {
                    Id = question.Id,
                    Answers = question.Answers,
                    Question = question.QuestionText,
                    Answer = null,
                    QuestionShort = question.QuestionTextShort
                };
                returnValue.Add(data);
            }

            this.status = 3;

            return returnValue;
        }

        public double GetQuestionsStatus()
        {
            double done = 0;
            foreach(Task<Question> question in this.questionsList)
            {
                if(question.Status == TaskStatus.RanToCompletion)
                {
                    done++;
                }
                else if (question.Status == TaskStatus.Running)
                {
                    done += 0.5;
                }
            }

            return done == 0 ? 0 : done / this.questionsList.Count;
        }

        /// <summary>
        /// Set answers for questions
        /// </summary>
        /// <param name="data">List of answers</param>
        public async Task SetAnswers(List<AnswerData> data)
        {
            if (this.status < 3)
            {
                throw new Exception("Not properly order of methods call");
            }
            this.answerData = Task.Run(() =>
            {
                return data;
            });
            this.answers = Task.Run(async () =>
            {
                return (await Task.WhenAll(this.questionsList)).Where(x => x != null).ToList();
            });
            AnswerData aData;
            foreach(Question question in await this.answers)
            {
                aData = data.Find(x => x.Id == question.Id);
                question.IsImportant = aData.Important;
                question.Answer = aData.Answer;
            }

            this.status = 4;
        }

        /// <summary>
        /// Get answers of questions
        /// </summary>
        /// <returns>List of answered questions</returns>
        public async Task<List<QuestionData>> GetAnswered()
        {
            List<QuestionData> returnValue = new List<QuestionData>();

            foreach (Question answer in await this.answers)
            {
                returnValue.Add(new QuestionData()
                {
                    Id = answer.Id,
                    Answers = answer.Answers,
                    Question = answer.QuestionText,
                    Important = answer.IsImportant,
                    Answer = answer.Answer
                });
            }
            return returnValue;
        }

        /// <summary>
        /// Set favorite places
        /// </summary>
        /// <param name="places">List of places</param>
        public void SetFavoritePlaces(List<FavoritePlaceData> places) //TODO add this to properly methods flow
        {
            if (this.status < 4)
            {
                throw new Exception("Not properly order of methods call");
            }

            this.favoritePlaces = Task.Run(() =>
            {
                return places;
            });

            Task<List<ExternalData>> localRealEstates = this.realEstates;

            this.realEstates = Task.Run(async () =>
            {
                return rating.RateFavoritePlaces(await localRealEstates, await this.favoritePlaces);
            });

            this.status = 5;
        }

        /// <summary>
        /// Get list of favorite places
        /// </summary>
        /// <returns>List of favorite places</returns>
        public async Task<List<FavoritePlaceData>> GetFavoritePlaces() //TODO add this to properly methods flow
        {
            if (this.status < 4)
            {
                throw new Exception("Not properly order of methods call");
            }

            this.status = 5;
            return await this.favoritePlaces;
        }

        /// <summary>
        /// Get result of search
        /// </summary>
        /// <returns>All results of rated and compared real estaes</returns>
        public async Task<List<ExternalData>> GetResult()
        {
            if (this.status < 5)
            {
                throw new Exception("Not properly order of methods call");
            }

            if(
                this.realEstates.Status == TaskStatus.Faulted ||
                (await this.realEstates) == null ||
                (await this.realEstates).Count < 1
               )
            {
                throw new Exception("No data");
            }

            if (this.result == null || !this.result.IsCompleted)
            {
                throw new Exception("Not done yet");
            }

            this.status = 7;
            return await this.result;
        }

        /// <summary>
        /// Get one resuld with interest data
        /// </summary>
        /// <param name="id">Id of real estate</param>
        /// <returns>Real estate</returns>
        public async Task<ExternalData> GetResult(string id)
        {
            if (this.status < 5)
            {
                throw new Exception("Not properly order of methods call");
            }

            if (
                this.realEstates.Status == TaskStatus.Faulted ||
                (await this.realEstates) == null ||
                (await this.realEstates).Count < 1
               )
            {
                throw new Exception("No data");
            }

            if (this.result == null || !this.result.IsCompleted)
            {
                throw new Exception("Not done yet");
            };

            ExternalData data = (await this.result).Find(x => x.Id == id);

            data = interestModel.GetInterest((RealEstate)data);

            return data;
        }

        /// <summary>
        /// Save actual answers and location
        /// </summary>
        public async Task<string> Save(string email)
        {
            string hash = save.SaveData(new ToSaveData()
            {
                Location = await this.searchLocation,
                Answers = await this.answerData,
                FavoritPlaces = await this.favoritePlaces
            }, email);

            Email sender = new Email();
            sender.SendSavedHash(hash, email);

            return hash;
        }

        /// <summary>
        /// Load saved answers and location
        /// </summary>
        /// <param name="hash">hash to identify save</param>
        public async Task<string> Load(string hash)
        {
            ToSaveData data = save.Load(hash);
            if (data != null)
            {
                this.SetLocation(data.Location);
                await this.SearchQuestions();
                this.status = 3;
                await this.SetAnswers(data.Answers);
                this.SetFavoritePlaces(data.FavoritPlaces);
                await this.SearchResult();
                save.Status = "done";
                return "done";
            }
            else
            {
                save.Status = "failed";
                return "failed";
            }
        }

        /// <summary>
        /// Get status of loading (if is done or not)
        /// </summary>
        public string LoadStatus()
        {
            if (save.Status == "failed")
            {
                throw new Exception("Not found");
            }
            else if (save.Status != "done")
            {
                throw new Exception("Not done yet");
            }

            return "done";
        }

        /// <summary>
        /// Get status of searching
        /// </summary>
        /// <returns>Name of actual step</returns>
        public string GetStatus()
        {
            return statusLabels[status];
        }
    }
}