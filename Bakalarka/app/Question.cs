﻿using Bakalarka.app.Communication;
using Bakalarka.app.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakalarka.app
{
    /// <summary>
    /// Question class to contain question, answer, map data for question..
    /// </summary>
    public abstract class Question
    {
        public int? Answer { get; set; } = null;
        public bool IsImportant { get; set; } = false;
        protected List<ExternalData> mapData = new List<ExternalData>();
        protected LocationData location;
        private readonly System.Threading.EventWaitHandle waitHandleValid = new System.Threading.AutoResetEvent(false);
        private readonly System.Threading.EventWaitHandle waitHandleData = new System.Threading.AutoResetEvent(false);

        private string _QuestionTextShort;
        public string QuestionTextShort
        {
            get
            {
                return this._QuestionTextShort;
            }
            set
            {
                this._QuestionTextShort = value;
            }
        }
        protected abstract string SetQuestionTextShort();

        private string _QuestionText;
        public string QuestionText {
            get
            {
                return this._QuestionText;
            }
            set
            {
                this._QuestionText = value;
            }
        }
        protected abstract string SetQuestionText();

        private List<Tuple<int, string>> _Answers;
        public List<Tuple<int, string>> Answers {
            get
            {
                return this._Answers;
            }
            set
            {
                this._Answers = value;
            }
        }
        protected abstract List<Tuple<int, string>> SetAnswers();

        private string _Id;
        public string Id {
            get
            {
                return this._Id;
            }
            set
            {
                this._Id = value;
            }
        }
        protected abstract string SetId();


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="location">Location where to search</param>
        public Question(LocationData location)
        {
            IsValid = false;
            this.QuestionText = SetQuestionText();
            this.QuestionTextShort = SetQuestionTextShort();
            this.Answers = SetAnswers();
            this.Id = SetId();
            this.ResetWaitHandles();
        }

        /// <summary>
        /// Reset all WaitHandles
        /// </summary>
        private void ResetWaitHandles()
        {
            this.waitHandleData.Reset();
            this.waitHandleValid.Reset();
        }

        /// <summary>
        /// Get if question have any POI in search area
        /// </summary>
        /// <returns>true if have more than 0 POI</returns>
        private bool _IsValid;
        public bool IsValid
        {
            get
            {
                this.waitHandleValid.WaitOne();
                this.waitHandleValid.Set();

                return this._IsValid;
            }

            set
            {
                this._IsValid = value;
            }
        }

        /// <summary>
        /// Get data from external resource
        /// </summary>
        /// <param name="data">lambda expression for get data</param>
        protected void GetExternalData(Func<List<ExternalData>> data)
        {
            this.ResetWaitHandles();
            new Task(() =>
            {
                this.mapData = data().GroupBy(x => new { x.Location, x.Name }).Select(x => x.First()).ToList();
                this.waitHandleData.Set();
                this.IsValid = this.mapData.Count > 0;
                this.waitHandleValid.Set();
            }).Start();
        }

        /// <summary>
        /// Get POI data for question
        /// </summary>
        /// <returns>List of poi</returns>
        public List<ExternalData> GetData()
        {
            this.waitHandleData.WaitOne();
            this.waitHandleData.Set();
            return this.mapData;
        }
    }
}