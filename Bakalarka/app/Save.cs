﻿using Bakalarka.app.Communication;
using Bakalarka.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bakalarka.app
{
    /// <summary>
    /// Save or load list to/from database
    /// </summary>
    public class Save
    {
        private SaveModel db = new SaveModel();
        public string Status { get; set; } = "";

        /// <summary>
        /// Load list from database
        /// </summary>
        /// <param name="hash">Hash of saved record</param>
        /// <returns>Saved data</returns>
        public ToSaveData Load(string hash)
        {
            var saveQuery = 
                from s in db.Save
                where s.Hash == hash && s.Active
                select s;

            SaveSet save;
            try
            {
                save = saveQuery.Single<SaveSet>();
            }
            catch
            {
                return null;
            }

            List<AnswerData> answersData = new List<AnswerData>();
            foreach(AnswerSet set in save.Answer)
            {
                answersData.Add(new AnswerData()
                {
                    Answer = set.Answer,
                    Id = set.QuestionId,
                    Important = set.Important
                });
            }

            List<FavoritePlaceData> favoritPlacesData = new List<FavoritePlaceData>();
            int index = 0;
            foreach (FavoritPlaceSet fa in save.FavoritPlace)
            {
                favoritPlacesData.Add(new FavoritePlaceData()
                {
                    Id = index,
                    Location = new LocationData() { lat = fa.LatLocation, lng = fa.LngLocation},
                    Type = fa.Type,
                    Name = fa.Name
                });
                index++;
            }


            return new ToSaveData()
            {
               
                Location = new LocationData() { lat = save.LatLocation, lng = save.LngLocation, radius = save.Radius },
                Answers = answersData,
                FavoritPlaces = favoritPlacesData
            };
        }

        /// <summary>
        /// Save data to database
        /// </summary>
        /// <param name="data">Data to save</param>
        /// <param name="email">Email of customer where to send url access</param>
        /// <returns>Hash of record</returns>
        public string SaveData(ToSaveData data, string email)
        {
            string hash = Guid.NewGuid().ToString().Substring(0, 8);

            ICollection<AnswerSet> answersSet = new List<AnswerSet>();
            foreach(AnswerData aData in data.Answers)
            {
                answersSet.Add(new AnswerSet()
                {
                    Answer = aData.Answer,
                    QuestionId = aData.Id,
                    Important = aData.Important,
                });
            }

            ICollection<FavoritPlaceSet> favoritPlacesSet = new List<FavoritPlaceSet>();
            foreach (FavoritePlaceData fData in data.FavoritPlaces)
            {
                favoritPlacesSet.Add(new FavoritPlaceSet()
                {
                    LatLocation = fData.Location.lat,
                    LngLocation = fData.Location.lng,
                    Type = fData.Type,
                    Name = fData.Name
                });
            }

            SaveSet save = new SaveSet()
            {
                Email = email,
                Hash = hash,
                Active = true,
                LatLocation = data.Location.lat,
                LngLocation = data.Location.lng,
                Radius = data.Location.radius,
                Answer = answersSet,
                FavoritPlace = favoritPlacesSet
            };

            db.Save.Add(save);
            db.SaveChangesAsync();

            return hash;
        }

        /// <summary>
        /// Remove record from dabase
        /// </summary>
        /// <param name="hash">Hash of record</param>
        /// <param name="email">Email of record</param>
        /// <returns>status</returns>
        public string Delete(string hash, string email)
        {
            var removeQuery =
                from s in db.Save
                where s.Hash == hash && s.Email == email && s.Active
                select s;

            SaveSet save;
            try
            {
                save = removeQuery.Single<SaveSet>();
            }
            catch
            {
                return "notFound";
            }

            save.Active = false;

            db.SaveChangesAsync();

            return "done";
        }
    }
}