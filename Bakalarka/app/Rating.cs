﻿using Bakalarka.app.Communication;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Bakalarka.app
{
    /// <summary>
    /// Rate real estates for each question and  give value to each real estate 
    /// </summary>
    public class Rating
    {

        /// <summary>
        /// Rate real estates by answers list for question
        /// </summary>
        /// <param name="realEstates">List of real estates</param>
        /// <param name="questions">List of questions</param>
        /// <returns>List of rated reale estates</returns>
        public List<ExternalData> RateQuestions(List<ExternalData> realEstates, List<Question> questions)
        {
            List<DistanceMapDataTemp> actualRealEstateCollection = new List<DistanceMapDataTemp>();
            int answer = 0;
            int answerCount = 0;
            int realEstateCount = 0;
            int delimiterCeil = 0;
            int delimiterFloor = 0;
            int delimiterAnswer = 0;
            double delimiterDouble = 0;
            double shortestDistance = Double.MaxValue;
            MapPOI shortestMapPoi = null;
            DistanceMapDataTemp actualDistanceData = null;
            double distance = 0;

            foreach (RealEstate realEastate in realEstates)
                realEastate.PoiData.Clear();

            foreach (Question question in questions)
            {

                foreach (RealEstate realEastate in realEstates)
                {

                    foreach (MapPOI mapPoi in question.GetData())
                    {
                        distance = Helpers.GetDistance(realEastate.Location.getCoords(), mapPoi.Location.getCoords());
                        if(distance < shortestDistance)
                        {
                            shortestDistance = distance;
                            shortestMapPoi = mapPoi;
                        }
                    }

                    if(shortestDistance != Double.MaxValue)
                        actualRealEstateCollection.Add(new DistanceMapDataTemp()
                        {
                            Distance = shortestDistance,
                            Realestate = realEastate,
                            MapPoi = shortestMapPoi
                        });
                    shortestDistance = Double.MaxValue;
                    shortestMapPoi = null;
                }

                actualRealEstateCollection.Sort((x, y) => x.Distance.CompareTo(y.Distance));

                answerCount = question.Answers.Count;
                realEstateCount = actualRealEstateCollection.Count;
                answer = answerCount;
                delimiterDouble = (double)realEstateCount / answerCount;
                delimiterCeil = (int)Math.Ceiling((double)realEstateCount / answerCount);
                delimiterFloor = (int)Math.Floor((double)realEstateCount / answerCount);
                delimiterAnswer = delimiterDouble % 1 > 0.7 ? delimiterCeil : delimiterFloor;
                //max is 10 answers
                delimiterAnswer = delimiterAnswer == 0 ? 1 : delimiterAnswer;

                for (int i = 0; i < realEstateCount; i++)
                {
                    actualDistanceData = actualRealEstateCollection.ElementAt(i);
                    actualDistanceData.Realestate.PoiData.Add(new PoiData()
                    {
                        Id = question.Id,
                        Answer = answer,
                        Question = question.QuestionText,
                        QuestionShort = question.QuestionTextShort,
                        Location = actualDistanceData.MapPoi.Location,
                        Address = actualDistanceData.MapPoi.Address,
                        Icon = actualDistanceData.MapPoi.Icon,
                        Name = actualDistanceData.MapPoi.Name,
                        Distance = (int)actualDistanceData.Distance
                    });
                    if (answer > 1 && i % delimiterAnswer == delimiterAnswer - 1)
                    {
                        answer--;
                    }
                }
                actualRealEstateCollection.Clear();
            }

           
            return realEstates;
        }

        /// <summary>
        /// Rate real estates by favorite places selections via Google Matrix api
        /// If are requests to Google Matrix api not allowed any more it will use air distance
        /// </summary>
        /// <param name="realEstates">List of real estates</param>
        /// <param name="places">List of favorite places</param>
        /// <returns>List of rated reale estates</returns>
        public List<ExternalData> RateFavoritePlaces(List<ExternalData> realEstates, List<FavoritePlaceData> places)
        {
            if (places == null)
            {
                return realEstates;
            }

            foreach (RealEstate realEastate in realEstates)
                realEastate.FavoritePlace.Clear();

            List<DistanceFavoritPlaceDataTemp> placeRealEstateCollection = new List<DistanceFavoritPlaceDataTemp>();
            List<DistanceFavoritPlaceDataTemp> placeRealEstateCollectionActual;
            int rate = 1;
            int delimiterPoint = 0;
            int realEstateCount = 0;
            int delimiterCeil = 0;
            int delimiterFloor = 0;
            int categoryCount = 3; //Select favorite places this categories
            double delimiterDouble = 0;
            DistanceFavoritPlaceDataTemp actualElement;
            string destinationDriving = "";
            string destinationWalking = "";
            string origins = "";
            string key = ConfigurationManager.AppSettings["googleApiKey"];

            foreach (FavoritePlaceData place in places)
            {
                if (place.Type == "driving")
                {
                    destinationDriving += place.Location.lat.ToString().Replace(',', '.') + ',' + place.Location.lng.ToString().Replace(',', '.') + '|';
                }
                else if (place.Type == "walking")
                {
                    destinationWalking += place.Location.lat.ToString().Replace(',', '.') + ',' + place.Location.lng.ToString().Replace(',', '.') + '|';
                }
            }

            foreach (RealEstate realEastate in realEstates)
            {
                origins += realEastate.Location.lat.ToString().Replace(',', '.') + ',' + realEastate.Location.lng.ToString().Replace(',', '.') + '|';
            }

            if(origins == "")
            {
                return realEstates;
            }

            origins = origins.Remove(origins.Length - 1);

            GoogleMatrixApi apiResultDriving = null;
            GoogleMatrixApi apiResultWalking = null;

            DateTime today = DateTime.Today;
            int daysUntilMonday = ((int)DayOfWeek.Monday - (int)today.DayOfWeek + 7) % 7;
            DateTime nextMonday = today.AddDays(daysUntilMonday);
            DateTime nextMondayDriving = nextMonday.Date + new TimeSpan(7, 30, 0); // Next monday 7:30
            DateTime nextMondayWalking = nextMonday + new TimeSpan(14, 0, 0); // Next monday 14:00
            int drivingTimestamp = (Int32)(nextMondayDriving.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            int walkingTimestamp = (Int32)(nextMondayWalking.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            if (destinationDriving != "")
            {
                destinationDriving = destinationDriving.Remove(destinationDriving.Length - 1);
                try
                {
                    apiResultDriving = JsonConvert.DeserializeObject<GoogleMatrixApi>(Helpers.Get($"https://maps.googleapis.com/maps/api/distancematrix/json?mode=driving&departure_time={drivingTimestamp}&origins={origins}&destinations={destinationDriving}&key={key}"));

                    if (apiResultDriving.status != "OK")
                    {
                        placeRealEstateCollection.AddRange(this.ProccessDataFromApi(null, realEstates, places, "driving"));
                    }
                    else
                    {
                        placeRealEstateCollection.AddRange(this.ProccessDataFromApi(apiResultDriving, realEstates, places, "driving"));
                    }
                }
                catch
                {
                    placeRealEstateCollection.AddRange(this.ProccessDataFromApi(apiResultDriving, realEstates, places, "driving"));
                }
            }

            if (destinationWalking != "")
            {
                destinationWalking = destinationWalking.Remove(destinationWalking.Length - 1);

                try
                {
                    apiResultWalking = JsonConvert.DeserializeObject<GoogleMatrixApi>(Helpers.Get($"https://maps.googleapis.com/maps/api/distancematrix/json?mode=walking&departure_time={walkingTimestamp}&origins={origins}&destinations={destinationWalking}&key={key}"));

                    if (apiResultWalking.status != "OK")
                    {
                        placeRealEstateCollection.AddRange(this.ProccessDataFromApi(null, realEstates, places, "walking"));
                    }
                    else
                    {
                        placeRealEstateCollection.AddRange(this.ProccessDataFromApi(apiResultWalking, realEstates, places, "walking"));
                    }
                }
                catch
                {
                    placeRealEstateCollection.AddRange(this.ProccessDataFromApi(apiResultWalking, realEstates, places, "walking"));
                }
            }

            foreach (FavoritePlaceData place in places)
            {
                placeRealEstateCollectionActual = placeRealEstateCollection.FindAll(x => x.FavoritPlace.Equals(place));
                placeRealEstateCollectionActual.Sort((x, y) => x.Time.CompareTo(y.Time));
                
                realEstateCount = placeRealEstateCollectionActual.Count;
                rate = categoryCount;
                delimiterDouble = (double)realEstateCount / categoryCount;
                delimiterCeil = (int)Math.Ceiling((double)realEstateCount / categoryCount);
                delimiterFloor = (int)Math.Floor((double)realEstateCount / categoryCount);
                delimiterPoint = delimiterDouble % 1 > 0.7 ? delimiterCeil : delimiterFloor;
                //max is 10 favorite places
                delimiterPoint = delimiterPoint == 0 ? 1 : delimiterPoint;

                for (int i = 0; i < placeRealEstateCollectionActual.Count; i++)
                {
                    actualElement = placeRealEstateCollectionActual.ElementAt(i);
                    actualElement.Realestate.FavoritePlace.Add(new FavoritePlaceData
                    {
                        Id = actualElement.FavoritPlace.Id,
                        Location = actualElement.FavoritPlace.Location,
                        Name = actualElement.FavoritPlace.Name??"",
                        Rate = rate,
                        Type = actualElement.FavoritPlace.Type
                    });
                    if (rate > 1 && i % delimiterPoint == delimiterPoint - 1)
                        rate--;
                }

                actualElement = null;
                placeRealEstateCollectionActual.Clear();
            }

            return realEstates;
        }

        /// <summary>
        /// Process data from google matrix api
        /// </summary>
        /// <param name="data">Data from google api</param>
        /// <param name="realEstates">List of real estates</param>
        /// <param name="places">List of favorite places</param>
        /// <param name="type">Type of travel</param>
        /// <returns>List of distances from favorite place and real estate</returns>
        private List<DistanceFavoritPlaceDataTemp> ProccessDataFromApi(GoogleMatrixApi data, List<ExternalData> realEstates, List<FavoritePlaceData> places, string type)
        {
            List<DistanceFavoritPlaceDataTemp> list = new List<DistanceFavoritPlaceDataTemp>();
            FavoritePlaceData placeActual;
            int indexRealEstate = 0;
            int indexFavoritPlace = 0;

            if (data == null)
            {
                foreach (ExternalData realestate in realEstates)
                {

                    foreach (FavoritePlaceData place in places)
                    {
                        if(place.Type == type)
                        {
                            list.Add(new DistanceFavoritPlaceDataTemp()
                            {
                                Time = Helpers.GetDistance(realestate.Location.getCoords(), place.Location.getCoords()),
                                Realestate = (RealEstate)realestate,
                                FavoritPlace = place
                            });
                        }
                    }
                }

                return list;
            }
            

            foreach (RowMatrixApi row in data.rows)
            {
                indexFavoritPlace = 0;
                foreach (ElementMatrixApi element in row.elements)
                {
                    if (element.status == "OK")
                    {
                        placeActual = places.FindAll(x => x.Type == type)[indexFavoritPlace];
                        placeActual.Name = data.destination_addresses[indexFavoritPlace];
                        list.Add(new DistanceFavoritPlaceDataTemp()
                        {
                            Time = element.duration_in_traffic == null ? element.duration.value : element.duration_in_traffic.value,
                            Realestate = (RealEstate)realEstates[indexRealEstate],
                            FavoritPlace = placeActual
                        });
                    }

                    indexFavoritPlace++;
                }

                indexRealEstate++;
            }
            indexRealEstate = 0;
            indexFavoritPlace = 0;

            return list;
        }
    }

    /// <summary>
    /// Temporary class for holding data in Rating.RateQuestions()
    /// </summary>
    class DistanceMapDataTemp
    {
        public double Distance { get; set; }
        public RealEstate Realestate { get; set; }
        public MapPOI MapPoi { get; set; }
    }

    /// <summary>
    /// Temporary class for holding data in Rating.RateFavoritePlaces()
    /// </summary>
    class DistanceFavoritPlaceDataTemp
    {
        public double Time { get; set; }
        public RealEstate Realestate { get; set; }
        public FavoritePlaceData FavoritPlace { get; set; }
    }
}