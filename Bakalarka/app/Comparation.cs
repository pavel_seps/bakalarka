﻿using Bakalarka.app.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bakalarka.app
{
    /// <summary>
    /// Comparation class to compare rated real estates and answered questions
    /// </summary>
    public class Comparation
    {
        /// <summary>
        /// Compare lists of rated real estates and answered questions
        /// </summary>
        /// <param name="realEstates">Rated real estates</param>
        /// <param name="questions">Answered questions</param>
        /// <param name="favoritePlacesCount">Count of favorite places</param>
        /// <returns>Real estates with percent match with questions. Can return null on empty real estates or questions</returns>
        public List<ExternalData> Compare(List<ExternalData> realEstates, List<Question> questions, int favoritePlacesCount)
        {
            List<Question> answeredQuestions = questions.Where(x => x.Answer != null && x.IsValid).ToList();
            double correctResult = 0;
            int correctImportant = 0;
            int questionsCount = answeredQuestions.Count;
            int totalCount = favoritePlacesCount + questionsCount;
            double onePointPercent = (100 / totalCount);
            double importantMultiply = onePointPercent / 3; //Importatn question = add 1/3 of question value to important

            if (realEstates.Count < 1 || questionsCount < 1)
            {
                return null;
            }

            foreach (RealEstate realEstate in realEstates)
            {
                correctResult = 0;
                correctImportant = 0;
                foreach (Question answeredQuestion in answeredQuestions)
                {
                    PoiData resultRealEstate = realEstate.PoiData.Find(x => x.Id == answeredQuestion.Id);

                    if(resultRealEstate != null)
                    {
                        if (resultRealEstate.Answer == answeredQuestion.Answer)
                        {
                            correctResult++;

                            if (answeredQuestion.IsImportant)
                            {
                                correctImportant++;
                            }
                        }
                    }
                }
                
                foreach(FavoritePlaceData place in realEstate.FavoritePlace)
                {
                    switch (place.Rate)
                    {
                        case 2:
                            correctResult += 0.5;
                            break;
                        case 3:
                            correctResult++;
                            break;
                    }
                }

                realEstate.Percentage = (int)Math.Round((correctResult*onePointPercent));
                realEstate.Percentage += (int)(importantMultiply * correctImportant);
                realEstate.Percentage = realEstate.Percentage > 100 ? 100 : realEstate.Percentage;
            }

            return realEstates;
        }
    }
}