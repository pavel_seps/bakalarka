﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bakalarka.app
{
    /// <summary>
    /// Map point of interest
    /// </summary>
    public class MapPOI : ExternalData
    {
        public string Address { get; set; }
        public string Icon { get; set; }
    }
}