﻿using Bakalarka.app.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakalarka.app.Interface
{
    /// <summary>
    /// Interface for external connection (POI/Real Estate)
    /// </summary>
    interface DataConnection
    {
        List<ExternalData> GetData(LocationData location);
    }
}
