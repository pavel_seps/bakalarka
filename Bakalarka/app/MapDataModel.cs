﻿using Bakalarka.app.Communication;
using Bakalarka.app.Interface;
using Bakalarka.app.MapModels.Questions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;

namespace Bakalarka.app
{
    /// <summary>
    /// Unite all resources for questions
    /// </summary>
    public class MapDataModel
    {

        /// <summary>
        /// Get questions from all resources
        /// </summary>
        /// <param name="location">Location where to search</param>
        /// <returns>List of questions</returns>
        public List<Task<Question>> GetQuestions(LocationData location)
        {
            List<Task<Question>> resultCollection = new List<Task<Question>>();

            var types = Assembly
                  .GetExecutingAssembly()
                  .GetTypes()
                  .Where(t => (t.Namespace == null ? "" : t.Namespace).Contains("MapModels.Questions") && t.MemberType.ToString().Contains("TypeInfo"));


            foreach (var t in types)
            {
                try
                {
                    resultCollection.Add(Task<Question>.Run(() =>
                    {
                        Question actualQuestion = (Question)Activator.CreateInstance(t, location);
                        return actualQuestion;
                    }));
                }
                catch
                {
                    throw new Exception("Not used right interface in RealEsateModels");
                }
            }


            
            return resultCollection;
        }
    }
}