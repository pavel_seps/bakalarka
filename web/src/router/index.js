import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/view/Index'
import UserDoc from '@/view/UserDoc'
import ApiDoc from '@/view/ApiDoc'
import Search from '@/view/Search'
import Load from '@/view/Load'
import Remove from '@/view/Remove'
import ListAddCustom from '@/view/Add/List'
import NewFormAddCustom from '@/view/Add/New'
import EditFormAddCustom from '@/view/Add/Form'
import PageNotFound from '@/view/Error/404'
import ServerError from '@/view/Error/500'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Index',
            component: Index
        },
        {
            path: '/user-documentation',
            name: 'UserDoc',
            component: UserDoc
        },
        {
            path: '/api-documentation',
            name: 'ApiDoc',
            component: ApiDoc
        },
        {
            path: '/search',
            name: 'Search',
            component: Search
        },
        {
            path: '/save/load/:hash',
            name: 'Load',
            component: Load
        },
        {
            path: '/save/remove/:hash',
            name: 'Remove',
            component: Remove
        },
        {
            path: '/add-custom',
            name: 'ListAddCustom',
            component: ListAddCustom
        },
        {
            path: '/add-custom/new',
            name: 'NewFormAddCustom',
            component: NewFormAddCustom
        },
        {
            path: '/add-custom/:type/:id',
            name: 'EditFormAddCustom',
            component: EditFormAddCustom
        },
        {
            path: '/500',
            name: '500',
            component: ServerError
        },
        {
            path: '/*',
            name: '404',
            component: PageNotFound
        }
    ],
    mode: 'history'
})
