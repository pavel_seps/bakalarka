import axios from 'axios';

export default () => {
    return axios.create({
        baseURL: process.env.SERVER_URL,
        timeout: 10000*6,
        withCredentials: true
    });
}