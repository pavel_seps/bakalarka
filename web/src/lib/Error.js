export default class Error {
    constructor(router, message) {
        if(process.env.NODE_ENV === "development"){
            console.log(message);
        }else{
            router.push({ name: '500'});
        }
    }
}