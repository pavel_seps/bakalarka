'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
    NODE_ENV: '"development"', //Don't change this!
    SERVER_URL: '"http://localhost:49655"',
    GOOGLE_MAP_API_KEY: '""',
    GOOGLE_MAP_API_KEY_GEOCODING: '""'
})
