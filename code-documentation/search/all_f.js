var searchData=
[
  ['save',['Save',['../class_bakalarka_1_1app_1_1_save.html',1,'Bakalarka.app.Save'],['../class_bakalarka_1_1app_1_1_search.html#acf97c286d712d54ac0e9c6f520be0314',1,'Bakalarka.app.Search.Save()']]],
  ['savecontroller',['SaveController',['../class_bakalarka_1_1_controllers_1_1_save_controller.html',1,'Bakalarka::Controllers']]],
  ['savedata',['SaveData',['../class_bakalarka_1_1app_1_1_communication_1_1_save_data.html',1,'Bakalarka.app.Communication.SaveData'],['../class_bakalarka_1_1app_1_1_save.html#a1bdad575a5dfba730e2ee908034c383a',1,'Bakalarka.app.Save.SaveData()']]],
  ['savemodel',['SaveModel',['../class_bakalarka_1_1_models_1_1_save_model.html',1,'Bakalarka::Models']]],
  ['saveset',['SaveSet',['../class_bakalarka_1_1_models_1_1_save_set.html',1,'Bakalarka::Models']]],
  ['search',['Search',['../class_bakalarka_1_1app_1_1_search.html',1,'Bakalarka.app.Search'],['../class_bakalarka_1_1app_1_1_search.html#ae9bc03ffbedadea04c6674ec9d1cdd4d',1,'Bakalarka.app.Search.Search()']]],
  ['searchcontroller',['SearchController',['../class_bakalarka_1_1_controllers_1_1_search_controller.html',1,'Bakalarka::Controllers']]],
  ['searchresult',['SearchResult',['../class_bakalarka_1_1app_1_1_search.html#a6a10b2b21907a00bedbdff94f5dca1ef',1,'Bakalarka::app::Search']]],
  ['sendsavedhash',['SendSavedHash',['../class_bakalarka_1_1app_1_1_email.html#a7f777c4985fb2777aead0e0057b80649',1,'Bakalarka::app::Email']]],
  ['setanswers',['SetAnswers',['../class_bakalarka_1_1app_1_1_search.html#a8fd3fe0aa14ad1d93e224befc0b2b6ab',1,'Bakalarka::app::Search']]],
  ['setfavoriteplaces',['SetFavoritePlaces',['../class_bakalarka_1_1app_1_1_search.html#a0497267238f4141c9f54803b6d2fc843',1,'Bakalarka::app::Search']]],
  ['setlocation',['SetLocation',['../class_bakalarka_1_1app_1_1_search.html#a51acfd8f782ccb51b25dedcbf782e4fb',1,'Bakalarka::app::Search']]],
  ['stringify',['Stringify',['../class_bakalarka_1_1app_1_1_communication_1_1_transfer.html#a65655cc974676a6dda63176d56d54e1b',1,'Bakalarka.app.Communication.Transfer.Stringify()'],['../class_bakalarka_1_1app_1_1_communication_1_1_transfer.html#a65655cc974676a6dda63176d56d54e1b',1,'Bakalarka.app.Communication.Transfer.Stringify()']]]
];
